JASPAR Data Processing
======================

This distribution provides tools to consume curation data provided for a
JASPAR release, preparing metadata and structuring data for publication via
the JASPAR Web interface.

Previously, JASPAR data was prepared using the `jaspar_2020`_ software. For
data prepared from 2021 onwards - including the 2022 release of JASPAR - a
different software distribution is used. Note, however, that this software
distribution (as opposed to `jaspar_2020`_) is also able to produce archives
of data for older JASPAR releases. Such archives can then be deployed with
more recent versions of the JASPAR Web interface software.

.. _`jaspar_2020`: https://bitbucket.org/CBGR/jaspar_2020


Processing Environment and Prerequisites
----------------------------------------

This software has been run in a server environment with Python 3.8 and R 4.0.2
made available as follows:

.. code-block:: shell

   scl enable rh-python38 bash
   module load R/4.0.2

The ``deploy.sh`` script can be used to install these and other software
prerequisites for this activity, with the following command requiring elevated
privileges and attempting to install appropriate Python and R distributions:

.. code-block:: shell

   ./deploy.sh

Where certain system-level prerequisites can be met using techniques such as
those shown above (``scl`` and ``module load``), making Python and R
available in an alternative fashion, additional prerequisites can be
introduced by running the ``deploy.sh`` script in an unprivileged mode:

.. code-block:: shell

   ./deploy.sh -u

This should ensure that various necessary Python and R packages are installed
for use in the processing activity.

Preparing Import-Ready Data and Metadata
----------------------------------------

The given ``Makefile`` attempts to process curation data within an environment
providing the accompanying data files and to produce data and metadata in
appropriate forms for Web publication. Help on the different operations
supported by the ``Makefile`` can be obtained as follows:

.. code-block:: shell

   make help

The ``Makefile`` can be run as follows from the top level of this
distribution:

.. code-block:: shell

   make

This is equivalent to the following:

.. code-block:: shell

   make JASPAR_DATA_DIR=../JASPAR/data

The ``JASPAR_DATA_DIR`` argument can be explicitly specified, indicating the
location of various inputs to the data preparation process.

Since TFFM data may be computed after the new release data has been processed,
a separate step is needed to prepare the TFFM data for Web publication:

.. code-block:: shell

   make tffm

The ``INCOMING_TFFM_DIR`` argument can be explicitly specified to indicate
where TFFM data has been situated.

Once processing is complete, data and metadata archives (including TFFM
archived) are deployed as follows:

.. code-block:: shell

   make install
   make install_tffm

This form of invocation is equivalent to the following:

.. code-block:: shell

   make OUTPUT_DATA=/var/tmp/jaspar-data install
   make OUTPUT_DATA=/var/tmp/jaspar-data install_tffm

The ``OUTPUT_DATA`` argument can be explicitly specified, and this is
recommended if the data is to be copied to a different machine for deployment.

Once processing is complete, data and metadata will be written to the
indicated (or default) directory.

Having prepared data for a release of the database, the need may arise to
revise the data. Given appropriate inputs, revised data archives can be
prepared as follows:

.. code-block:: shell

   make revised

This can then be followed by use of the ``install_revised`` target as follows:

.. code-block:: shell

   make install_revised

The ``OUTPUT_DATA`` argument can be specified as for the ``install`` target.

Where TFFM data has been regenerated for the revised data, use the following
commands:

.. code-block:: shell

   make tffm_revised
   make install_tffm_revised

Again, the ``OUTPUT_DATA`` argument can be specified.

There may also be additional fixes issued even for the revised data. These can
be applied as follows:

.. code-block:: shell

   make fixed
   make install_fixed

Once again, the ``OUTPUT_DATA`` argument can be specified.


Processing Details
------------------

Information about the processing done by this distribution can be found in the
dataflow_ document.

.. _dataflow: docs/Dataflow.rst


Copyright and Licensing Information
-----------------------------------

Source code originating in this project is licensed under the terms of the GNU
General Public License version 3 or later (GPLv3 or later). Original content
is licensed under the Creative Commons Attribution Share Alike 4.0
International (CC-BY-SA 4.0) licensing terms.

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this software's source code distribution for complete copyright and licensing
information, including the licensing details of bundled code and content.
