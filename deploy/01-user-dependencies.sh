#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Initialise local dependencies.

python3 -m pip install --ignore-installed --root "$BASEDIR/lib" -r "$BASEDIR/requirements.txt"

mkdir -p "$BASEDIR/lib/usr/local/lib/R/library"
R_LIBS="$BASEDIR/lib/usr/local/lib/R/library" Rscript "$BASEDIR/requirements.R"
