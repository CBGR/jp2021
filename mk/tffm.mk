# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Process incoming TFFM data, combining it with existing TFFM data, and
# exporting the data for eventual archiving.



# Process TFFM data.

$(UPDATED_TFFM_METADATA_MARKER): $(UPDATED_METADATA_MARKER) $(INCOMING_METADATA_TFFM)
	$(DATATOOLS)/process_tffm_data $(DATABASE) $(INCOMING_METADATA_TFFM) && \
	touch $@

# Process TFFM data for any revision of the new release.

$(UPDATED_TFFM_METADATA_REVISED_MARKER): $(UPDATED_METADATA_REVISED_MARKER) $(INCOMING_METADATA_TFFM)
	$(DATATOOLS)/process_tffm_data $(DATABASE_REVISED) $(INCOMING_METADATA_TFFM) && \
	touch $@



# Combine existing TFFM data with TFFM data for the new release.

$(INCOMING_TFFM_MANIFEST): $(JASPAR_INCOMING_TFFM_DATA_CACHED) | $(INCOMING_TFFM_DIR)
	$(TOOLS)/tar_extract $(JASPAR_INCOMING_TFFM_DATA_CACHED) $(INCOMING_TFFM_DIR) && \
	touch $@

$(TFFM_COMPLETE_DATA_MARKER): $(UPDATED_TFFM_FILES) $(JASPAR_EXISTING_TFFM_DATA_CACHED) | $(TFFM_COMPLETE_DATA) $(STATUS)
	$(TOOLS)/tar_extract $(JASPAR_EXISTING_TFFM_DATA_CACHED) $(TFFM_COMPLETE_DATA) && \
	$(DATATOOLS)/export_tffm_files $(UPDATED_TFFM_FILES) $(INCOMING_TFFM_DIR) $(TFFM_COMPLETE_DATA) && \
	touch $@

# Produce TFFM data for any revision of the new release.

$(TFFM_COMPLETE_DATA_MARKER_REVISED): $(UPDATED_TFFM_FILES_REVISED) $(JASPAR_EXISTING_TFFM_DATA_CACHED) | $(TFFM_COMPLETE_DATA_REVISED) $(STATUS)
	$(TOOLS)/tar_extract $(JASPAR_EXISTING_TFFM_DATA_CACHED) $(TFFM_COMPLETE_DATA_REVISED) && \
	$(DATATOOLS)/export_tffm_files $(UPDATED_TFFM_FILES_REVISED) $(INCOMING_TFFM_DIR) $(TFFM_COMPLETE_DATA_REVISED) && \
	touch $@
