# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Propagate matrix history to an earlier database release.

# Usage:
# $(eval $(call matrix_history_template,RELEASE,SOURCE_RELEASE))

# Examples:
# $(eval $(call matrix_history_template,2020,2022))
# $(eval $(call matrix_history_template,$(RELEASE),$(JASPAR_RELEASE_NEW)))



define matrix_history_template =

$(METADATA)/$(1)/matrix_history: $(METADATA)/$(2)/matrix_history | $(METADATA)/$(1)
	cp $(METADATA)/$(2)/matrix_history $(METADATA)/$(1)/matrix_history

endef
