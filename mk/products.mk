# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Data types involved in this processing activity.
# TFFM data is handled separately.

DATA_TYPES			= annotations data profiles proteins species
ARCHIVED_DATA_TYPES		= $(DATA_TYPES) data_labelled
EXISTING_DATA_TYPES		= $(DATA_TYPES) data_labelled
INCOMING_DATA_TYPES		= $(DATA_TYPES) actions files
UPDATED_DATA_TYPES		= $(DATA_TYPES) data_labelled files history



# Processing artefacts.

ALL_DATA			= $(WORK)/data
ALL_DATABASES			= $(WORK)/databases
ALL_DATA_TFFM			= $(WORK)/data_tffm
CLUSTERING_DATA			= $(WORK)/clustering
DOWNLOAD_DATA			= $(WORK)/download
INCOMING_CLUSTERING_DATA	= $(WORK)/clustering_incoming
INCOMING_METADATA		= $(WORK)/incoming
INCOMING_METADATA_FIXED		= $(WORK)/incoming_fixed
INCOMING_METADATA_REVISED	= $(WORK)/incoming_revised
INCOMING_METADATA_TFFM		= $(WORK)/tffm_incoming_metadata
INCOMING_TFFM_DIR		= $(WORK)/tffm_incoming
LOGOS				= $(WORK)/logos
METADATA			= $(WORK)/metadata
METADATA_TFFM			= $(WORK)/metadata_tffm
MISSING_DATA_FILES		= $(WORK)/missing_data_files
MISSING_DATA_FILES_REVISED	= $(WORK)/missing_data_files_revised
MISSING_DATA_FILES_FIXED	= $(WORK)/missing_data_files_fixed
NAMING_DATA			= $(WORK)/naming
STATUS				= $(WORK)/status
SYNONYMS			= $(WORK)/synonyms
SYNONYMS_WORK			= $(WORK)/synonyms_work
TAXONOMY_DATA			= $(WORK)/taxonomy_data
TFFM_COMPLETE_DATA		= $(WORK)/tffm_complete
TFFM_COMPLETE_DATA_REVISED	= $(WORK)/tffm_complete_revised
WORDCLOUDS			= $(WORK)/wordclouds

# Specific marker files indicating processing status.

TFFM_COMPLETE_DATA_MARKER		= $(STATUS)/_tffm_complete_
TFFM_COMPLETE_DATA_MARKER_REVISED	= $(STATUS)/_tffm_complete_revised_
WORDCLOUDS_MARKER			= $(STATUS)/_wordclouds_

# Markers are used to indicate database status, with an additional TFFM
# processing stage undertaken on the new release.

UPDATED_METADATA_MARKER		= $(STATUS)/_metadata_updated_.$(JASPAR_RELEASE_NEW)
UPDATED_TFFM_METADATA_MARKER	= $(STATUS)/_tffm_updated_.$(JASPAR_RELEASE_NEW)

# Revision of the data employs another status indicator.

UPDATED_METADATA_REVISED_MARKER		= $(STATUS)/_metadata_updated_.$(JASPAR_RELEASE_NEW_REVISED)
UPDATED_TFFM_METADATA_REVISED_MARKER	= $(STATUS)/_tffm_updated_.$(JASPAR_RELEASE_NEW_REVISED)

# The database being built is labelled with the new release year.

DATABASE			= $(ALL_DATABASES)/JASPAR$(JASPAR_RELEASE_NEW).sqlite3

# A revised version of the data employs a modified version of the database.

DATABASE_REVISED		= $(ALL_DATABASES)/JASPAR$(JASPAR_RELEASE_NEW_REVISED).sqlite3

# A fixed version of the revised data.

DATABASE_FIXED			= $(ALL_DATABASES)/JASPAR$(JASPAR_RELEASE_NEW_FIXED).sqlite3

# Converted incoming metadata for the new release.

INCOMING_CURATION_DATA		= $(INCOMING_METADATA)/incoming_curation_data
INCOMING_DELETION_DATA		= $(INCOMING_METADATA)/matrix_deletions

# Converted revision metadata for the new release.

INCOMING_CURATION_REVISION_DATA	= $(INCOMING_METADATA_REVISED)/incoming_curation_data
INCOMING_DELETION_REVISION_DATA	= $(INCOMING_METADATA_REVISED)/matrix_deletions

# TFFM data location.

INCOMING_TFFM_MANIFEST		= $(INCOMING_TFFM_DIR)/results/table_centrality_pvalues_passing_threshold.tsv

# Locations of metadata for the latest and new releases, since these releases
# are the ones involved the most in processing.

EXISTING_METADATA		= $(METADATA)/$(JASPAR_RELEASE_LATEST)
EXISTING_METADATA_TFFM		= $(METADATA_TFFM)/$(JASPAR_RELEASE_LATEST)
UPDATED_METADATA		= $(METADATA)/$(JASPAR_RELEASE_NEW)
UPDATED_METADATA_TFFM		= $(METADATA_TFFM)/$(JASPAR_RELEASE_NEW)

# Location of revised metadata for the new release.

UPDATED_METADATA_REVISED	= $(METADATA)/$(JASPAR_RELEASE_NEW_REVISED)
UPDATED_METADATA_TFFM_REVISED	= $(METADATA_TFFM)/$(JASPAR_RELEASE_NEW_REVISED)

# Location of fixed, revised metadata.

UPDATED_METADATA_FIXED		= $(METADATA)/$(JASPAR_RELEASE_NEW_FIXED)

# Existing metadata for the latest release, along with curation-derived metadata
# for the new release.

EXISTING_MATRIX_DETAILS		= $(foreach DETAILS,$(DATA_TYPES),$(EXISTING_METADATA)/matrix_$(DETAILS))
INCOMING_MATRIX_DETAILS		= $(foreach DETAILS,$(INCOMING_DATA_TYPES),$(INCOMING_METADATA)/matrix_$(DETAILS))

# Revised metadata for the new release.

INCOMING_MATRIX_DETAILS_REVISED	= $(foreach DETAILS,$(INCOMING_DATA_TYPES),$(INCOMING_METADATA_REVISED)/matrix_$(DETAILS))

# Location of matrix-related data files for the latest release. These
# provide BED, centrality and site details.

EXISTING_MATRIX_FILES		= $(EXISTING_METADATA)/matrix_files

# Naming-related data used to generate gene synonym information.

SYNONYM_TABLE			= $(NAMING_DATA)/synonyms.table
UNIPROT_ACCESSIONS		= $(NAMING_DATA)/uniprot_accessions
UNIPROT_GENES           	= $(NAMING_DATA)/uniprot_genes

# Taxonomy-related data used to classify profile species, along with source data
# used to construct the taxonomy groups required.

TAX_GROUP_MEMBERS		= $(TAXONOMY_DATA)/tax_group_members
TAX_DETAILS			= $(TAXONOMY_DATA)/tax_names $(TAXONOMY_DATA)/tax_names_ext

# Matrix data: archived, latest release and new release.

ARCHIVED_DATA			= $(ALL_DATA)/archived
EXISTING_DATA			= $(ALL_DATA)/$(JASPAR_RELEASE_LATEST)
UPDATED_DATA			= $(ALL_DATA)/$(JASPAR_RELEASE_NEW)

# Revised matrix data for the new release.

UPDATED_DATA_REVISED		= $(ALL_DATA)/$(JASPAR_RELEASE_NEW_REVISED)

# Fixed, revised data.

UPDATED_DATA_FIXED		= $(ALL_DATA)/$(JASPAR_RELEASE_NEW_FIXED)

# Existing TFFM metadata.

EXISTING_MATRIX_TFFM		= $(EXISTING_METADATA_TFFM)/matrix_tffm

# TFFM data for the new release.

UPDATED_TFFM_DATA		= $(UPDATED_DATA)/tffm

# TFFM metadata supporting the new data.

UPDATED_TFFM_FILES		= $(UPDATED_METADATA_TFFM)/tffm_files
UPDATED_MATRIX_TFFM		= $(UPDATED_METADATA_TFFM)/matrix_tffm

# TFFM metadata supporting the revised data.

UPDATED_TFFM_FILES_REVISED	= $(UPDATED_METADATA_TFFM_REVISED)/tffm_files
UPDATED_MATRIX_TFFM_REVISED	= $(UPDATED_METADATA_TFFM_REVISED)/matrix_tffm

# Eventual outputs.

UPDATED_CHANGELOG_DATA		= $(WORK)/changelog_$(JASPAR_RELEASE_NEW).html
UPDATED_CHANGELOG_REVISION_DATA	= $(WORK)/changelog_$(JASPAR_RELEASE_NEW)_revised.html
UPDATED_MATRIX_HISTORY		= $(WORK)/jaspar-matrix-history-$(JASPAR_RELEASE_NEW).tsv
UPDATED_MATRIX_HISTORY_REVISED	= $(WORK)/jaspar-matrix-history-$(JASPAR_RELEASE_NEW_REVISED).tsv



# Common archives labelled with the new release.

ARCHIVE_ARCHIVED_PROFILES	= $(WORK)/matrix_archived_$(JASPAR_RELEASE_NEW).tar.gz
ARCHIVE_DATABASE		= $(WORK)/JASPAR$(JASPAR_RELEASE_NEW).sql.gz
ARCHIVE_LOGOS			= $(WORK)/matrix_logos_$(JASPAR_RELEASE_NEW).tar.gz
ARCHIVE_TFFM			= $(WORK)/matrix_tffm_$(JASPAR_RELEASE_NEW).tar.gz
ARCHIVE_WORDCLOUDS		= $(WORK)/matrix_wordclouds_$(JASPAR_RELEASE_NEW).tar.gz

# Revised data archives.

ARCHIVE_ARCHIVED_PROFILES_REVISED = $(WORK)/matrix_archived_$(JASPAR_RELEASE_NEW_REVISED).tar.gz
ARCHIVE_DATABASE_REVISED	= $(WORK)/JASPAR$(JASPAR_RELEASE_NEW_REVISED).sql.gz
ARCHIVE_LOGOS_REVISED		= $(WORK)/matrix_logos_$(JASPAR_RELEASE_NEW_REVISED).tar.gz
ARCHIVE_TFFM_REVISED		= $(WORK)/matrix_tffm_$(JASPAR_RELEASE_NEW_REVISED).tar.gz

# Release-specific archives.

ARCHIVE_ALL_DATA		= $(foreach RELEASE,$(JASPAR_ALL_RELEASES),$(WORK)/matrix_data_$(RELEASE).tar.gz)
ARCHIVE_CLUSTERING		= $(foreach RELEASE,$(JASPAR_ALL_CLUSTERING_RELEASES),$(WORK)/matrix_clustering_$(RELEASE).tar.gz)
ARCHIVE_DOWNLOADS		= $(foreach RELEASE,$(JASPAR_ALL_RELEASES),$(WORK)/matrix_download_$(RELEASE).tar.gz)
ARCHIVE_METADATA		= $(foreach RELEASE,$(JASPAR_ALL_RELEASES),$(WORK)/matrix_metadata_$(RELEASE).tar.gz)
ARCHIVE_SYNONYMS		= $(foreach RELEASE,$(JASPAR_ALL_RELEASES),$(WORK)/matrix_synonyms_$(RELEASE).tar.gz)
ARCHIVE_TFFM_DATA		= $(foreach RELEASE,$(JASPAR_ALL_TFFM_RELEASES),$(WORK)/matrix_data_tffm_$(RELEASE).tar.gz)
ARCHIVE_TFFM_METADATA		= $(foreach RELEASE,$(JASPAR_ALL_TFFM_RELEASES),$(WORK)/matrix_metadata_tffm_$(RELEASE).tar.gz)

# More revised data archives.

ARCHIVE_ALL_DATA_REVISED	= $(WORK)/matrix_data_$(JASPAR_RELEASE_NEW_REVISED).tar.gz
ARCHIVE_DOWNLOADS_REVISED	= $(WORK)/matrix_download_$(JASPAR_RELEASE_NEW_REVISED).tar.gz
ARCHIVE_METADATA_REVISED	= $(foreach RELEASE,$(JASPAR_ALL_RELEASES_REVISED),$(WORK)/matrix_metadata_$(RELEASE).tar.gz)
ARCHIVE_SYNONYMS_REVISED	= $(WORK)/matrix_synonyms_$(JASPAR_RELEASE_NEW_REVISED).tar.gz
ARCHIVE_TFFM_DATA_REVISED	= $(WORK)/matrix_data_tffm_$(JASPAR_RELEASE_NEW_REVISED).tar.gz
ARCHIVE_TFFM_METADATA_REVISED	= $(WORK)/matrix_metadata_tffm_$(JASPAR_RELEASE_NEW_REVISED).tar.gz

# Even more fixed data archives.

ARCHIVE_ALL_DATA_FIXED		= $(WORK)/matrix_data_$(JASPAR_RELEASE_NEW_FIXED).tar.gz



# Summary of outputs.

OUTPUTS_NO_BACKUP		= $(CHANGELOG_DATA_CACHED) \
				  $(CURATION_DATA_CACHED) \
				  $(JASPAR_ARCHETYPES_DATA_CACHED) \
				  $(JASPAR_ARCHIVED_DATA_CACHED) \
				  $(JASPAR_MATRIX_HISTORY_CACHED) \
				  $(JASPAR_SITE_DATA_CACHED) \
				  $(TAX_DATA_CACHED) \
				  $(GENE_INFO_CACHED) \
				  $(UNIPROT_SPROT_CACHED)

OUTPUTS_NO_BACKUP_REVISED	= $(CHANGELOG_REVISION_DATA_CACHED) \
				  $(CURATION_REVISION_DATA_CACHED)

OUTPUTS_NON_TFFM		= $(ARCHIVE_ARCHIVED_PROFILES) \
				  $(ARCHIVE_ALL_DATA) \
				  $(ARCHIVE_CLUSTERING) \
				  $(ARCHIVE_DATABASE) \
				  $(ARCHIVE_DOWNLOADS) \
				  $(ARCHIVE_LOGOS) \
				  $(ARCHIVE_METADATA) \
				  $(ARCHIVE_SYNONYMS) \
				  $(ARCHIVE_WORDCLOUDS) \
				  $(UPDATED_CHANGELOG_DATA) \
				  $(UPDATED_MATRIX_HISTORY)

OUTPUTS_NON_TFFM_REVISED	= $(ARCHIVE_ARCHIVED_PROFILES_REVISED) \
				  $(ARCHIVE_ALL_DATA_REVISED) \
				  $(ARCHIVE_DATABASE_REVISED) \
				  $(ARCHIVE_DOWNLOADS_REVISED) \
				  $(ARCHIVE_LOGOS_REVISED) \
				  $(ARCHIVE_METADATA_REVISED) \
				  $(ARCHIVE_SYNONYMS_REVISED) \
				  $(UPDATED_CHANGELOG_REVISION_DATA) \
				  $(UPDATED_MATRIX_HISTORY_REVISED)

OUTPUTS_NON_TFFM_FIXED		= $(ARCHIVE_ALL_DATA_FIXED)

OUTPUTS_TFFM			= $(ARCHIVE_TFFM) $(ARCHIVE_TFFM_DATA) \
				  $(ARCHIVE_TFFM_METADATA)

OUTPUTS_TFFM_REVISED		= $(ARCHIVE_TFFM_REVISED) $(ARCHIVE_TFFM_DATA_REVISED) \
				  $(ARCHIVE_TFFM_METADATA_REVISED)

OUTPUTS_ALL			= $(OUTPUTS_NON_TFFM) $(OUTPUTS_TFFM) \
				  $(OUTPUTS_NON_TFFM_REVISED) $(OUTPUTS_TFFM_REVISED) \
				  $(OUTPUTS_NON_TFFM_FIXED)

# Determine all outputs for creation and deletion purposes.

ALMOST_ALL_OUTPUT_DIRECTORIES	= $(ALL_DATA) \
				  $(ALL_DATA_TFFM) \
				  $(ALL_DATABASES) \
				  $(ARCHIVED_DATA) \
				  $(CLUSTERING_DATA) \
				  $(DOWNLOAD_DATA) \
				  $(INCOMING_CLUSTERING_DATA) \
				  $(INCOMING_METADATA) \
				  $(INCOMING_METADATA_FIXED) \
				  $(INCOMING_METADATA_REVISED) \
				  $(INCOMING_TFFM_DIR) \
				  $(LOGOS) \
				  $(METADATA) \
				  $(METADATA_TFFM) \
				  $(NAMING_DATA) \
				  $(STATUS) \
				  $(SYNONYMS) \
				  $(SYNONYMS_WORK) \
				  $(TAXONOMY_DATA) \
				  $(TFFM_COMPLETE_DATA) \
				  $(TFFM_COMPLETE_DATA_REVISED) \
				  $(UPDATED_DATA) \
				  $(UPDATED_DATA_FIXED) \
				  $(UPDATED_DATA_REVISED) \
				  $(WORDCLOUDS)

ALL_OUTPUT_DIRECTORIES		= $(ALMOST_ALL_OUTPUT_DIRECTORIES) \
				  $(JASPAR_DATA_DIR_CACHED)

ALMOST_ALL_OUTPUT_FILES		= $(INCOMING_METADATA_TFFM) \
				  $(MISSING_DATA_FILES) \
				  $(MISSING_DATA_FILES_REVISED) \
				  $(OUTPUTS_ALL)

ALL_OUTPUT_FILES		= $(ALMOST_ALL_OUTPUT_FILES) \
				  $(OUTPUTS_NO_BACKUP) \
				  $(OUTPUTS_NO_BACKUP_REVISED)

# All directories to be created during processing.

ALL_DIRECTORIES			= $(ALL_OUTPUT_DIRECTORIES) \
				  $(OUTPUT_DATA) \
				  $(WORK)

# Installed locations of the outputs.

OUTPUTS_INSTALLED_NO_BACKUP		= $(foreach OUTPUT,$(OUTPUTS_NO_BACKUP),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))
OUTPUTS_INSTALLED_NO_BACKUP_REVISED	= $(foreach OUTPUT,$(OUTPUTS_NO_BACKUP_REVISED),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))
OUTPUTS_INSTALLED_NON_TFFM		= $(foreach OUTPUT,$(OUTPUTS_NON_TFFM),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))
OUTPUTS_INSTALLED_NON_TFFM_REVISED	= $(foreach OUTPUT,$(OUTPUTS_NON_TFFM_REVISED),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))
OUTPUTS_INSTALLED_NON_TFFM_FIXED	= $(foreach OUTPUT,$(OUTPUTS_NON_TFFM_FIXED),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))
OUTPUTS_INSTALLED_TFFM			= $(foreach OUTPUT,$(OUTPUTS_TFFM),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))
OUTPUTS_INSTALLED_TFFM_REVISED		= $(foreach OUTPUT,$(OUTPUTS_TFFM_REVISED),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))

OUTPUTS_INSTALLED_ALL		= $(OUTPUTS_INSTALLED_NO_BACKUP) \
				  $(OUTPUTS_INSTALLED_NO_BACKUP_REVISED) \
				  $(OUTPUTS_INSTALLED_NON_TFFM) \
				  $(OUTPUTS_INSTALLED_NON_TFFM_REVISED) \
				  $(OUTPUTS_INSTALLED_NON_TFFM_FIXED) \
				  $(OUTPUTS_INSTALLED_TFFM) \
				  $(OUTPUTS_INSTALLED_TFFM_REVISED)
