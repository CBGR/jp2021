# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Taxonomy-related rules.

# Remote sources.

TAXDUMP_URL		= https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz

# Marker files to record build status.

TAX_DETAILS_MARKER	= $(STATUS)/_tax_converted_



# Download data or use existing downloads. Existing downloads will be referenced
# using symbolic links and will therefore not be reinstalled.

$(TAX_DATA_CACHED): | $(JASPAR_DATA_DIR_CACHED)
	@if [ -e "$(TAX_DATA)" ] ; then \
		$(LINK) `realpath $(TAX_DATA)` $(TAX_DATA_CACHED) ; \
	elif [ -e "$(TAX_DATA_OLD)" ] ; then \
		$(LINK) `realpath $(TAX_DATA_OLD)` $(TAX_DATA_CACHED) ; \
	else \
		wget -nv -P $(JASPAR_DATA_DIR_CACHED) $(TAXDUMP_URL) ; \
	fi



# Convert data from the taxonomy download.

$(eval $(call grouped_target,$(TAX_DETAILS),$(TAX_DETAILS_MARKER)))

$(TAX_DETAILS_MARKER): $(TAX_DATA_CACHED) | $(TAXONOMY_DATA) $(STATUS)
	$(DATATOOLS)/convert_taxonomy_details $(TAX_DATA_CACHED) $(TAXONOMY_DATA) && \
	touch $@



# Process taxonomy data.

$(TAX_GROUP_MEMBERS): $(TAX_DATA_CACHED) | $(TAXONOMY_DATA)
	$(DATATOOLS)/make_taxonomy_groups $(TAX_DATA_CACHED) $(TAX_GROUP_MEMBERS)
