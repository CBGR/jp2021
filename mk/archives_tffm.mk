# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Produce TFFM archive for a given JASPAR release.

# Usage:
# $(eval $(call archives_tffm_template,RELEASE))



define archives_tffm_template =

# Release-specific output archives.

ARCHIVE_TFFM_DATA_$(1)		= $(WORK)/matrix_data_tffm_$(1).tar.gz
ARCHIVE_TFFM_METADATA_$(1)	= $(WORK)/matrix_metadata_tffm_$(1).tar.gz

# Input files.

MATRIX_TFFM_$(1)		= $(METADATA_TFFM)/$(1)/matrix_tffm
TFFM_TABLE_$(1)			= $(ALL_DATA_TFFM)/$(1)/TFFM_table.csv

# Marker files indicating availability of processed data.

TFFM_DATA_MARKER_$(1)		= $(STATUS)/_tffm_data_.$(1)



# Make TFFM archives.

$$(ARCHIVE_TFFM_DATA_$(1)): $$(TFFM_DATA_MARKER_$(1)) $$(TFFM_TABLE_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_TFFM_DATA_$(1)) $(ALL_DATA_TFFM)/$(1)

$$(ARCHIVE_TFFM_METADATA_$(1)): $$(MATRIX_TFFM_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_TFFM_METADATA_$(1)) $(METADATA_TFFM)/$(1)

endef
