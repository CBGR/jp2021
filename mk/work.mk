# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Create data directories.

$(ALL_DATA)/%:
	mkdir -p $@

# Make the TFFM download data directory for each release.

$(ALL_TFFM_DATA)/%:
	mkdir -p $@

# Create clustering data directories.

$(CLUSTERING_DATA)/%:
	mkdir -p $@

$(INCOMING_CLUSTERING_DATA)/%:
	mkdir -p $@

# Create download data directories.

$(DOWNLOAD_DATA)/%:
	mkdir -p $@

# Make logo directories.

$(LOGOS)/%:
	mkdir -p $@

# Create metadata directories.

$(METADATA)/%:
	mkdir -p $@

# Make the TFFM metadata directory for each release.

$(METADATA_TFFM)/%:
	mkdir -p $@

# Make the synonyms directories for each release.

$(SYNONYMS)/%:
	mkdir -p $@

$(SYNONYMS_WORK)/%:
	mkdir -p $@
