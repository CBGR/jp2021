# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Provide revised older releases, these merely having updated matrix history.

# Usage:
# $(eval $(call revise_existing_template,RELEASE))

# Examples:
# $(eval $(call revise_existing_template,2020))
# $(eval $(call revise_existing_template,$(RELEASE)))



define revise_existing_template =

METADATA_EXPORTED_$(1)			= $(foreach DETAILS,$(EXISTING_DATA_TYPES),$(METADATA)/$(1)/matrix_$(DETAILS))
METADATA_EXPORTED_REVISED_$(1)		= $(foreach DETAILS,$(EXISTING_DATA_TYPES),$(METADATA)/$(1)_revised/matrix_$(DETAILS))
TAX_DETAILS_$(1)			= $(METADATA)/$(1)/tax_names $(METADATA)/$(1)/tax_names_ext
TAX_DETAILS_REVISED_$(1)		= $(METADATA)/$(1)_revised/tax_names $(METADATA)/$(1)_revised/tax_names_ext

METADATA_EXPORTED_MARKER_$(1)		= $(STATUS)/_metadata_exported_.$(1)
METADATA_EXPORTED_REVISED_MARKER_$(1)	= $(STATUS)/_metadata_exported_.$(1)_revised
TAX_DETAILS_REVISED_MARKER_$(1)		= $(STATUS)/_tax_.$(1)_revised

# Propagate metadata to the revised release.

$$(eval $$(call grouped_target,$$(METADATA_EXPORTED_REVISED_$(1)),$$(METADATA_EXPORTED_REVISED_MARKER_$(1))))

$$(METADATA_EXPORTED_REVISED_MARKER_$(1)): $$(METADATA_EXPORTED_MARKER_$(1)) | $(METADATA)/$(1)_revised
	cp $$(METADATA_EXPORTED_$(1)) $(METADATA)/$(1)_revised && \
	touch $$@

$$(eval $$(call grouped_target,$$(TAX_DETAILS_REVISED_$(1)),$$(TAX_DETAILS_REVISED_MARKER_$(1))))

$$(TAX_DETAILS_REVISED_MARKER_$(1)): $$(TAX_DETAILS_$(1)) | $(METADATA)/$(1)_revised
	cp $$(TAX_DETAILS_$(1)) $(METADATA)/$(1)_revised && \
	touch $$@

endef
