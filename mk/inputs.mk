# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# A description of input data from previous releases and for this release.
# This file must be included after the initial JASPAR release definitions.



JASPAR_EXISTING_TFFM_RELEASES	= $(JASPAR_RELEASES_WITH_TFFM) $(JASPAR_RELEASE_LATEST)
JASPAR_ALL_TFFM_RELEASES	= $(JASPAR_EXISTING_TFFM_RELEASES) $(JASPAR_RELEASE_NEW)

# Existing releases and existing-plus-new releases involving any data.

JASPAR_EXISTING_RELEASES	= $(JASPAR_RELEASES_PREVIOUS) $(JASPAR_RELEASE_LATEST)
JASPAR_ALL_RELEASES		= $(JASPAR_EXISTING_RELEASES) $(JASPAR_RELEASE_NEW)
JASPAR_EXISTING_RELEASES_REVISED = $(foreach RELEASE,$(JASPAR_EXISTING_RELEASES),$(RELEASE)_revised)
JASPAR_ALL_RELEASES_REVISED	= $(foreach RELEASE,$(JASPAR_ALL_RELEASES),$(RELEASE)_revised)

# PFM data is generated even for archived/removed profiles.

JASPAR_ALL_PFM_RELEASES		= $(JASPAR_ALL_RELEASES) $(JASPAR_RELEASE_NEW)_archived
JASPAR_PFM_RELEASES_REVISED	= $(JASPAR_RELEASE_NEW_REVISED) $(JASPAR_RELEASE_NEW_REVISED)_archived

# Existing metadata resources (to be found in the existing data location).

JASPAR_DATA_PREVIOUS		= $(foreach RELEASE,$(JASPAR_RELEASES_PREVIOUS),JASPAR$(RELEASE).sql.gz)
JASPAR_DATA_LATEST		= JASPAR$(JASPAR_RELEASE_LATEST).sql.gz

# Existing download data archives (to be found in the existing data location).

JASPAR_DOWNLOAD_DATA		= $(foreach RELEASE,$(JASPAR_EXISTING_RELEASES),jaspar-download-data-$(RELEASE).tar.gz)

# Archetypes and clustering data downloads including the new release.

JASPAR_ARCHETYPES_DATA		= $(foreach RELEASE,$(JASPAR_ALL_ARCHETYPES_RELEASES),jaspar-archetypes-$(RELEASE).tar.gz)
JASPAR_CLUSTERING_DATA		= $(foreach RELEASE,$(JASPAR_ALL_CLUSTERING_RELEASES),jaspar-clustering-data-$(RELEASE).tar)

# Archived profile details from the latest release plus site metadata ready for
# the new release, existing TFFM data, and incoming TFFM data. The
# gene2wordclouds resource originates from a GitHub repository and contains a
# snapshot of a software distribution containing wordcloud images. We are only
# interested in the images.

JASPAR_ARCHIVED_DATA		= jaspar-archived-profiles-$(JASPAR_RELEASE_LATEST).tar.gz
JASPAR_MATRIX_HISTORY		= jaspar-matrix-history-$(JASPAR_RELEASE_LATEST).tsv
JASPAR_SITE_DATA		= jaspar-site-data-$(JASPAR_RELEASE_NEW).sql
JASPAR_EXISTING_TFFM_DATA	= jaspar-static-data-tffm-$(JASPAR_RELEASE_LATEST).tar.gz
JASPAR_INCOMING_TFFM_DATA	= jaspar-tffm-data-$(JASPAR_RELEASE_NEW).tar.gz
JASPAR_WORDCLOUDS_DATA		= gene2wordclouds-$(JASPAR_RELEASE_NEW).zip

# All existing database archive filenames.

JASPAR_EXISTING_DATA		= $(JASPAR_DATA_PREVIOUS) $(JASPAR_DATA_LATEST)

# All additional data, referenced via symbolic links and provided explicitly for
# this new release or for previous releases.

JASPAR_ALL_DATA			= $(CHANGELOG_DATA) \
				  $(CURATION_DATA) \
				  $(CHANGELOG_REVISION_DATA) \
				  $(CURATION_REVISION_DATA) \
				  $(JASPAR_ARCHETYPES_DATA) \
				  $(JASPAR_ARCHIVED_DATA) \
				  $(JASPAR_CLUSTERING_DATA) \
				  $(JASPAR_DOWNLOAD_DATA) \
				  $(JASPAR_EXISTING_DATA) \
				  $(JASPAR_MATRIX_HISTORY) \
				  $(JASPAR_SITE_DATA) \
				  $(JASPAR_EXISTING_TFFM_DATA) \
				  $(JASPAR_INCOMING_TFFM_DATA) \
				  $(JASPAR_WORDCLOUDS_DATA)
