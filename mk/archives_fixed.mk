# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Produce a fixed data archive for the revised new release.
# This encompasses all activities involved in producing the archive.

# Input details.

CURATION_FIXED_DATA_TABLES	= JASPAR_2022_revision2_BED_FASTA_correction \
				  JASPAR_2022_revision3_BED_FASTA_correction

# Working and output files.

ARCHIVE_ALL_DATA_FIXED		= $(WORK)/matrix_data_$(JASPAR_RELEASE_NEW_FIXED).tar.gz
INCOMING_CURATION_DATA_FIXED	= $(foreach TABLE,$(CURATION_FIXED_DATA_TABLES),$(INCOMING_METADATA_FIXED)/$(TABLE))
UPDATED_MATRIX_FILES_FIXED	= $(UPDATED_METADATA_FIXED)/matrix_files

# Marker files to indicate availability of processed data.

ALL_DATA_MARKER_FIXED		= $(STATUS)/_data_.$(JASPAR_RELEASE_NEW_FIXED)
METADATA_EXPORTED_REVISED_MARKER = $(STATUS)/_metadata_exported_.$(JASPAR_RELEASE_NEW_REVISED)
PFM_DATA_MARKER_FIXED		= $(STATUS)/_pfm_.$(JASPAR_RELEASE_NEW_FIXED)
PFM_DATA_MARKER_REVISED		= $(STATUS)/_pfm_.$(JASPAR_RELEASE_NEW_REVISED)



# Obtain fixed data from the incoming curation data.

# Define a rule for each curation data table obtaining the data from the table
# and selecting the appropriate columns.

define fixed_data_template =

$(INCOMING_METADATA_FIXED)/$(1): $(CURATION_REVISION_DATA_CACHED) | $(INCOMING_METADATA_FIXED)
	$(DATATOOLS)/ods2tsv $(CURATION_REVISION_DATA_CACHED) $(1) \
	| $(DATATOOLS)/get_fixed_bed_fasta_files > $(INCOMING_METADATA_FIXED)/$(1)

endef

# Create rules for all curation data tables.

$(foreach TABLE,$(CURATION_FIXED_DATA_TABLES),\
$(eval $(call fixed_data_template,$(TABLE))))

# Make the metadata directory.

$(UPDATED_METADATA_FIXED):
	mkdir -p $@

# Fix the revised data, copying the revised database as the basis for
# processing.

$(UPDATED_MATRIX_FILES_FIXED): $(INCOMING_CURATION_DATA_FIXED) $(METADATA_EXPORTED_REVISED_MARKER) | $(UPDATED_METADATA_FIXED)
	cp $(DATABASE_REVISED) $(DATABASE_FIXED) && \
	$(DATATOOLS)/fix_bed_fasta_files $(DATABASE_FIXED) $(UPDATED_MATRIX_FILES_FIXED) $(INCOMING_CURATION_DATA_FIXED)

# Export fixed data.

$(ALL_DATA_MARKER_FIXED): $(UPDATED_MATRIX_FILES_FIXED) | $(UPDATED_DATA_FIXED) $(STATUS)
	$(RM) -r $(UPDATED_DATA_FIXED)/bed
	$(RM) -r $(UPDATED_DATA_FIXED)/fasta
	$(DATATOOLS)/export_matrix_files $(UPDATED_MATRIX_FILES_FIXED) $(UPDATED_DATA_FIXED) > $(MISSING_DATA_FILES_FIXED) && \
	touch $@

# Copy PFM data.

$(PFM_DATA_MARKER_FIXED): $(PFM_DATA_MARKER_REVISED)
	cp -R $(ALL_DATA)/$(JASPAR_RELEASE_NEW_REVISED)/pfm $(ALL_DATA)/$(JASPAR_RELEASE_NEW_FIXED)/pfm && \
	touch $@

# Make a general data archive for the fixed data based on the revised data.

$(ARCHIVE_ALL_DATA_FIXED): $(ALL_DATA_MARKER_FIXED) $(PFM_DATA_MARKER_FIXED)
	$(TOOLS)/tar_create $(ARCHIVE_ALL_DATA_FIXED) $(ALL_DATA)/$(JASPAR_RELEASE_NEW_FIXED)
