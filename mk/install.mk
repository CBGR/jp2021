# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# A template providing rules for installing files using the copy tool.

# Usage:
# $(eval $(call install_template,FILENAME))



define install_template =
OUTPUT_INSTALLED_$(1) = $(OUTPUT_DATA)/$(shell basename $(1))

$$(OUTPUT_INSTALLED_$(1)): $(1) | $(OUTPUT_DATA)
	$(TOOLS)/copy $(1) $(OUTPUT_DATA)
endef

define install_no_backup_template =
OUTPUT_INSTALLED_$(1) = $(OUTPUT_DATA)/$(shell basename $(1))

$$(OUTPUT_INSTALLED_$(1)): $(1) | $(OUTPUT_DATA)
	$(TOOLS)/copy --no-backup $(1) $(OUTPUT_DATA)
endef
