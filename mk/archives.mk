# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Produce download and metadata archives for a given JASPAR release.
# TFFM-related archives are prepared in the archives_tffm.mk file.

# Usage:
# $(eval $(call archives_template,RELEASE))



define archives_template =

# Release-specific output archives.

ARCHIVE_ALL_DATA_$(1)		= $(WORK)/matrix_data_$(1).tar.gz
ARCHIVE_DOWNLOAD_$(1)		= $(WORK)/matrix_download_$(1).tar.gz
ARCHIVE_METADATA_$(1)		= $(WORK)/matrix_metadata_$(1).tar.gz
ARCHIVE_SYNONYMS_$(1)		= $(WORK)/matrix_synonyms_$(1).tar.gz

# Archive member details.

TAX_DETAILS_$(1)		= $(METADATA)/$(1)/tax_names $(METADATA)/$(1)/tax_names_ext
TAX_GROUPS_$(1)			= $(METADATA)/$(1)/tax_groups
JASPAR_INFO_$(1)		= $(METADATA)/$(1)/portal_release

# Explicitly introduce the matrix history to earlier releases. This is copied by
# a rule elsewhere.

ARCHIVED_MATRIX_DEPS_$(1) 	= $(STATUS)/_metadata_exported_.$(1) $(METADATA)/$(1)/matrix_history
ARCHIVE_METADATA_DEPS_$(1)	= $$(ARCHIVED_MATRIX_DEPS_$(1)) $$(TAX_DETAILS_$(1)) $$(TAX_GROUPS_$(1)) $$(JASPAR_INFO_$(1))

# Marker files to indicate availability of processed data.

ALL_DATA_MARKER_$(1)		= $(STATUS)/_data_.$(1)
DOWNLOAD_DATA_MARKER_$(1)	= $(STATUS)/_download_data_.$(1)
PFM_DATA_MARKER_$(1)		= $(STATUS)/_pfm_.$(1)
SYNONYMS_EXPORTED_MARKER_$(1)	= $(STATUS)/_synonyms_exported_.$(1)
TAX_DETAILS_MARKER_$(1)		= $(STATUS)/_tax_.$(1)



# Make general data, download data and metadata archives.

$$(ARCHIVE_ALL_DATA_$(1)): $$(ALL_DATA_MARKER_$(1)) $$(PFM_DATA_MARKER_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_ALL_DATA_$(1)) $(ALL_DATA)/$(1)

$$(ARCHIVE_DOWNLOAD_$(1)): $$(DOWNLOAD_DATA_MARKER_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_DOWNLOAD_$(1)) $(DOWNLOAD_DATA)/$(1)

$$(ARCHIVE_METADATA_$(1)): $$(ARCHIVE_METADATA_DEPS_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_METADATA_$(1)) $(METADATA)/$(1)

$$(ARCHIVE_SYNONYMS_$(1)): $$(SYNONYMS_EXPORTED_MARKER_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_SYNONYMS_$(1)) $(SYNONYMS)/$(1)



# Copy taxonomy groups and version metadata to the release-specific metadata
# directory.

$$(TAX_GROUPS_$(1)): $(DATA)/tax_groups | $(METADATA)/$(1)
	cp $(DATA)/tax_groups $(METADATA)/$(1)

$$(JASPAR_INFO_$(1)): $(DATA)/portal_release | $(METADATA)/$(1)
	cp $(DATA)/portal_release $(METADATA)/$(1)

endef
