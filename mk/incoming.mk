# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Process incoming curation data, preparing metadata for further processing.

INCOMING_MATRIX_DETAILS_MARKER		= $(STATUS)/_incoming_
INCOMING_MATRIX_DETAILS_REVISED_MARKER	= $(STATUS)/_incoming_revised_



$(eval $(call grouped_target,$(INCOMING_MATRIX_DETAILS),$(INCOMING_MATRIX_DETAILS_MARKER)))

$(INCOMING_MATRIX_DETAILS_MARKER): $(INCOMING_CURATION_DATA) | $(STATUS)
	$(DATATOOLS)/incoming_matrix_details $(INCOMING_CURATION_DATA) $(INCOMING_METADATA) && \
	touch $@

$(eval $(call grouped_target,$(INCOMING_MATRIX_DETAILS_REVISED),$(INCOMING_MATRIX_DETAILS_REVISED_MARKER)))

$(INCOMING_MATRIX_DETAILS_REVISED_MARKER): $(INCOMING_CURATION_REVISION_DATA) | $(STATUS)
	$(DATATOOLS)/incoming_matrix_details $(INCOMING_CURATION_REVISION_DATA) $(INCOMING_METADATA_REVISED) && \
	touch $@

# Obtain a summary of the new TFFM data for processing.

$(INCOMING_METADATA_TFFM): $(INCOMING_TFFM_MANIFEST)
	$(DATATOOLS)/incoming_tffm_data $(INCOMING_TFFM_MANIFEST) $(INCOMING_TFFM_DIR) $(INCOMING_METADATA_TFFM)
