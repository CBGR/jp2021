# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Obtain data and metadata from existing JASPAR releases. This includes the
# metadata (from an SQL archive) which is "instantiated" as a database, the
# downloadable BED, centrality, PFM and sites data (from the main download data
# archive).

# TFFM data (from the separate TFFM download data archive) is handled by the
# tffm_data.mk template and managed separately.

# Note that the PFM data will be regenerated from the database. See the pfm.mk
# template for more details.

# Usage:
# $(eval $(call existing_data_template,RELEASE))

# Examples:
# $(eval $(call existing_data_template,2020))
# $(eval $(call existing_data_template,$(RELEASE)))



define existing_data_template =

# Inputs.

DATABASE_$(1)				= $(ALL_DATABASES)/JASPAR$(1).sqlite3
DATA_SQL_$(1)				= $(JASPAR_DATA_DIR_CACHED)/JASPAR$(1).sql.gz
DOWNLOAD_DATA_$(1)			= $(JASPAR_DATA_DIR_CACHED)/jaspar-download-data-$(1).tar.gz

# Outputs.

ALL_DATA_$(1)				= $(ALL_DATA)/$(1)

# Marker files tracking progress.

ALL_DATA_MARKER_$(1)			= $(STATUS)/_data_.$(1)
METADATA_UPDATED_MARKER_$(1)		= $(STATUS)/_metadata_updated_.$(1)



# Make usable databases from SQL dump files.

$$(METADATA_UPDATED_MARKER_$(1)): $$(DATA_SQL_$(1)) | $(ALL_DATABASES) $(STATUS)
	$(RM) $$(DATABASE_$(1))
	gunzip -c $$(DATA_SQL_$(1)) | sqlite3 $$(DATABASE_$(1)) && \
	touch $$@

# Unpack existing data for augmentation with PFM data plus any TFFM data and the
# TFFM table download.

$$(ALL_DATA_MARKER_$(1)): $$(DOWNLOAD_DATA_$(1)) | $$(ALL_DATA_$(1)) $(STATUS)
	$(TOOLS)/tar_extract $$(DOWNLOAD_DATA_$(1)) $$(ALL_DATA_$(1)) && \
	touch $$@

endef
