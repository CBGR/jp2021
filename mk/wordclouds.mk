# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Wordcloud-related rules.

# Select data from the archive for deployment.

WORDCLOUDS_ROOT = gene2wordclouds-master

$(WORDCLOUDS_MARKER): $(JASPAR_WORDCLOUDS_DATA_CACHED) | $(WORDCLOUDS) $(STATUS)
	$(RM) -r $(WORDCLOUDS)/*
	unzip -q $(JASPAR_WORDCLOUDS_DATA_CACHED) '$(WORDCLOUDS_ROOT)/JASPAR/*/figs/*' -d $(WORDCLOUDS) && \
	mv $(WORDCLOUDS)/$(WORDCLOUDS_ROOT)/JASPAR/* $(WORDCLOUDS) && \
	$(RM) -r $(WORDCLOUDS)/$(WORDCLOUDS_ROOT) && \
	touch $@
