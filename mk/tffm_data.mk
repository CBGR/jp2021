# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Export TFFM data from databases. Select TFFM data from the updated TFFM
# collection, writing it to the download data directory for the indicated
# release.

# Usage:
# $(eval $(call tffm_data_template,RELEASE[,SUFFIX]))

# Examples:
# $(eval $(call tffm_data_template,2022))
# $(eval $(call tffm_data_template,$(RELEASE)))
# $(eval $(call tffm_data_template,$(RELEASE),_revised))



define tffm_data_template =

# Output directories.

ALL_DATA_TFFM_$(1)$(2)			= $(ALL_DATA_TFFM)/$(1)$(2)
METADATA_TFFM_$(1)$(2)			= $(METADATA_TFFM)/$(1)$(2)
TFFM_DATA_$(1)$(2)			= $$(ALL_DATA_TFFM_$(1)$(2))/TFFM

# Input files and database.

DATABASE_$(1)$(2)			= $(ALL_DATABASES)/JASPAR$(1)$(2).sqlite3
MATRIX_TFFM_$(1)$(2)			= $$(METADATA_TFFM_$(1)$(2))/matrix_tffm

# Output file.

TFFM_TABLE_$(1)$(2)			= $$(ALL_DATA_TFFM_$(1)$(2))/TFFM_table.csv

# Marker files to track progress.

TFFM_DATA_MARKER_$(1)$(2)		= $(STATUS)/_tffm_data_.$(1)$(2)
TFFM_EXPORTED_MARKER_$(1)$(2)		= $(STATUS)/_tffm_exported_.$(1)$(2)

# Employ either unrevised or revised complete TFFM data.
# This should employ directories of the form...
#
# tffm_complete
# tffm_complete_revised
#
# ...and marker files of the form...
#
# _tffm_complete_
# _tffm_complete_revised_
#
# Note that the directories and marker files are not qualified with the release
# year. Use of the new release is implied.

TFFM_COMPLETE_DATA_$(1)$(2)		= $(WORK)/tffm_complete$(2)
TFFM_COMPLETE_DATA_MARKER_$(1)$(2)	= $(STATUS)/_tffm_complete$(2)_

# TFFM metadata for the new release relies on an additional processing step,
# whereas earlier releases can provide the TFFM metadata straight away.

ifeq ($(1),$(JASPAR_RELEASE_NEW))
EXPORT_FLAGS_$(1)$(2)			= --updated
TFFM_EXPORTED_$(1)$(2)			= $$(METADATA_TFFM_$(1)$(2))/matrix_tffm $$(METADATA_TFFM_$(1)$(2))/tffm_files
TFFM_UPDATED_MARKER_$(1)$(2)		= $(STATUS)/_tffm_updated_.$(1)$(2)
else
EXPORT_FLAGS_$(1)$(2)			= --existing
TFFM_EXPORTED_$(1)$(2)			= $$(METADATA_TFFM_$(1)$(2))/matrix_tffm
TFFM_UPDATED_MARKER_$(1)$(2)		= $(STATUS)/_metadata_updated_.$(1)$(2)
endif



# Export TFFM data from databases.

$$(eval $$(call grouped_target,$$(TFFM_EXPORTED_$(1)$(2)),$$(TFFM_EXPORTED_MARKER_$(1)$(2))))

$$(TFFM_EXPORTED_MARKER_$(1)$(2)): $$(TFFM_UPDATED_MARKER_$(1)$(2)) | $$(METADATA_TFFM_$(1)$(2))
	$(DATATOOLS)/export_tffm_details $$(EXPORT_FLAGS_$(1)$(2)) $$(DATABASE_$(1)$(2)) $$(METADATA_TFFM_$(1)$(2)) && \
	touch $$@

# Select TFFM download data from the complete, updated TFFM collection.

$$(TFFM_DATA_MARKER_$(1)$(2)): $$(TFFM_COMPLETE_DATA_MARKER_$(1)$(2)) $$(MATRIX_TFFM_$(1)$(2)) | $$(TFFM_DATA_$(1)$(2))
	$(DATATOOLS)/select_tffm_files $$(MATRIX_TFFM_$(1)$(2)) $$(TFFM_COMPLETE_DATA_$(1)$(2)) $$(TFFM_DATA_$(1)$(2)) && \
	touch $$@

# Export the TFFM table from databases.

$$(TFFM_TABLE_$(1)$(2)): $$(TFFM_UPDATED_MARKER_$(1)$(2))
	$(DATATOOLS)/export_tffm_table $$(EXPORT_FLAGS_$(1)$(2)) $$(DATABASE_$(1)$(2)) $$(TFFM_TABLE_$(1)$(2))

endef
