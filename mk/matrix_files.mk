# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Locations of matrix-related data files for the new releases. These provide
# BED, centrality and site details.

UPDATED_MATRIX_FILES		= $(UPDATED_METADATA)/matrix_files

# Location of revised matrix file metadata.

UPDATED_MATRIX_FILES_REVISED	= $(UPDATED_METADATA_REVISED)/matrix_files

# Marker files to track complete generation of the data.
# These are also defined in other Makefile templates, usually in a more general
# way applying to an arbitrary release.

EXISTING_DATA_MARKER		= $(STATUS)/_data_.$(JASPAR_RELEASE_LATEST)
UPDATED_DATA_MARKER		= $(STATUS)/_data_.$(JASPAR_RELEASE_NEW)
UPDATED_DATA_REVISED_MARKER	= $(STATUS)/_data_.$(JASPAR_RELEASE_NEW_REVISED)

# Obtain filenames from the existing archive.

$(EXISTING_MATRIX_FILES): $(EXISTING_DATA_MARKER) | $(EXISTING_METADATA)
	$(DATATOOLS)/existing_matrix_files $(EXISTING_DATA) $(EXISTING_MATRIX_FILES)

# Export processed data using file metadata from the new release.
# See: export_data_template

$(UPDATED_DATA_MARKER): $(UPDATED_MATRIX_FILES) | $(UPDATED_DATA) $(STATUS)
	$(DATATOOLS)/export_matrix_files $(UPDATED_MATRIX_FILES) $(UPDATED_DATA) > $(MISSING_DATA_FILES) && \
	touch $@

# Export revised data for the new release.

$(UPDATED_DATA_REVISED_MARKER): $(UPDATED_MATRIX_FILES_REVISED) | $(UPDATED_DATA_REVISED) $(STATUS)
	$(DATATOOLS)/export_matrix_files $(UPDATED_MATRIX_FILES_REVISED) $(UPDATED_DATA_REVISED) > $(MISSING_DATA_FILES_REVISED) && \
	touch $@
