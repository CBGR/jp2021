# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Export data from a JASPAR release database.

# Usage:
# $(eval $(call export_data_template,RELEASE[,SUFFIX]))

# Examples:
# $(eval $(call export_data_template,2020))
# $(eval $(call export_data_template,$(RELEASE)))
# $(eval $(call export_data_template,$(RELEASE),_revised))



define export_data_template =

DATABASE_$(1)$(2)			= $(ALL_DATABASES)/JASPAR$(1)$(2).sqlite3
TAX_DETAILS_$(1)$(2)			= $(METADATA)/$(1)$(2)/tax_names $(METADATA)/$(1)$(2)/tax_names_ext

# Export archived matrix details when processing the new release, also in its
# revised form.

ifeq ($(1),$(JASPAR_RELEASE_NEW))
ARCHIVED_METADATA_$(1)$(2)		= $(METADATA)/$(1)$(2)_archived
EXPORT_FLAGS_$(1)$(2)			= --updated
METADATA_EXPORTED_$(1)$(2)_archived	= $(foreach DETAILS,$(ARCHIVED_DATA_TYPES),$$(ARCHIVED_METADATA_$(1)$(2))/matrix_$(DETAILS))
METADATA_EXPORTED_$(1)$(2)		= $(foreach DETAILS,$(UPDATED_DATA_TYPES),$(METADATA)/$(1)$(2)/matrix_$(DETAILS))
else
EXPORT_FLAGS_$(1)$(2)			= --existing
METADATA_EXPORTED_$(1)$(2)		= $(foreach DETAILS,$(EXISTING_DATA_TYPES),$(METADATA)/$(1)$(2)/matrix_$(DETAILS))
endif

# Marker files to handle collections of file dependencies.

ARCHIVED_METADATA_EXPORTED_MARKER_$(1)$(2)	= $(STATUS)/_metadata_exported_.$(1)$(2)_archived
DOWNLOAD_DATA_MARKER_$(1)$(2)			= $(STATUS)/_download_data_.$(1)$(2)
METADATA_EXPORTED_MARKER_$(1)$(2)		= $(STATUS)/_metadata_exported_.$(1)$(2)
METADATA_UPDATED_MARKER_$(1)$(2)		= $(STATUS)/_metadata_updated_.$(1)$(2)
TAX_DETAILS_MARKER_$(1)$(2)			= $(STATUS)/_tax_.$(1)$(2)



# Generate download data for this release.

$$(DOWNLOAD_DATA_MARKER_$(1)$(2)): $$(METADATA_UPDATED_MARKER_$(1)$(2)) | $(DOWNLOAD_DATA)/$(1)$(2)
	$(DATATOOLS)/make_archives $$(DATABASE_$(1)$(2)) $(DOWNLOAD_DATA)/$(1)$(2) $(1) && \
	touch $$@



# Export metadata from the databases.

$$(eval $$(call grouped_target,$$(METADATA_EXPORTED_$(1)$(2)),$$(METADATA_EXPORTED_MARKER_$(1)$(2))))

$$(METADATA_EXPORTED_MARKER_$(1)$(2)): $$(METADATA_UPDATED_MARKER_$(1)$(2)) | $(METADATA)/$(1)$(2)
	$(DATATOOLS)/export_matrix_details $$(EXPORT_FLAGS_$(1)$(2)) --number-annotations $$(DATABASE_$(1)$(2)) $(METADATA)/$(1)$(2) && \
	touch $$@

$$(eval $$(call grouped_target,$$(TAX_DETAILS_$(1)$(2)),$$(TAX_DETAILS_MARKER_$(1)$(2))))

$$(TAX_DETAILS_MARKER_$(1)$(2)): $$(METADATA_UPDATED_MARKER_$(1)$(2)) | $(METADATA)/$(1)$(2)
	$(DATATOOLS)/export_tax_details $$(EXPORT_FLAGS_$(1)$(2)) $$(DATABASE_$(1)$(2)) $(METADATA)/$(1)$(2) && \
	touch $$@



# For the new release, archived metadata records are exported to provide
# historical information.

ifeq ($(1),$(JASPAR_RELEASE_NEW))

$$(eval $$(call grouped_target,$$(METADATA_EXPORTED_$(1)$(2)_archived),$$(ARCHIVED_METADATA_EXPORTED_MARKER_$(1)$(2))))

$$(ARCHIVED_METADATA_EXPORTED_MARKER_$(1)$(2)): $$(METADATA_UPDATED_MARKER_$(1)$(2)) | $$(ARCHIVED_METADATA_$(1)$(2))
	$(DATATOOLS)/export_matrix_details --archived --number-annotations $$(DATABASE_$(1)$(2)) $$(ARCHIVED_METADATA_$(1)$(2)) && \
	touch $$@

endif

endef
