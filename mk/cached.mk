# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Inputs for processing that are cached in a directory for convenience.

JASPAR_DATA_DIR_CACHED		= $(WORK)/JASPAR_data

# Common data and metadata in the cached data location.

CHANGELOG_DATA_CACHED		= $(JASPAR_DATA_DIR_CACHED)/$(CHANGELOG_DATA)
CURATION_DATA_CACHED		= $(JASPAR_DATA_DIR_CACHED)/$(CURATION_DATA)
CHANGELOG_REVISION_DATA_CACHED	= $(JASPAR_DATA_DIR_CACHED)/$(CHANGELOG_REVISION_DATA)
CURATION_REVISION_DATA_CACHED	= $(JASPAR_DATA_DIR_CACHED)/$(CURATION_REVISION_DATA)
JASPAR_ARCHETYPES_DATA_CACHED	= $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_ARCHETYPES_DATA)
JASPAR_ARCHIVED_DATA_CACHED	= $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_ARCHIVED_DATA)
JASPAR_MATRIX_HISTORY_CACHED	= $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_MATRIX_HISTORY)
JASPAR_SITE_DATA_CACHED		= $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_SITE_DATA)
JASPAR_EXISTING_TFFM_DATA_CACHED = $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_EXISTING_TFFM_DATA)
JASPAR_INCOMING_TFFM_DATA_CACHED = $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_INCOMING_TFFM_DATA)
JASPAR_WORDCLOUDS_DATA_CACHED	= $(JASPAR_DATA_DIR_CACHED)/$(JASPAR_WORDCLOUDS_DATA)

# Additional data in the cached data location.

TAX_DATA_CACHED			= $(JASPAR_DATA_DIR_CACHED)/taxdump.tar.gz
GENE_INFO_CACHED		= $(JASPAR_DATA_DIR_CACHED)/gene_info.gz
UNIPROT_SPROT_CACHED		= $(JASPAR_DATA_DIR_CACHED)/uniprot_sprot.dat.gz

# Inputs explicitly recorded for processing.

TAX_DATA			= $(JASPAR_DATA_DIR)/taxdump.tar.gz
GENE_INFO			= $(JASPAR_DATA_DIR)/gene_info.gz
UNIPROT_SPROT			= $(JASPAR_DATA_DIR)/uniprot_sprot.dat.gz

# Inputs from any previous processing.
# This is a convenience to allow the processing to be performed on the same
# data again, where no inputs are recorded for processing.

JASPAR_DATA_DIR_OLD		= $(OUTPUT_DATA)
TAX_DATA_OLD			= $(OUTPUT_DATA)/taxdump.tar.gz
GENE_INFO_OLD			= $(OUTPUT_DATA)/gene_info.gz
UNIPROT_SPROT_OLD		= $(OUTPUT_DATA)/uniprot_sprot.dat.gz
