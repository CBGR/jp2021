# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Reference indicated or previously used data via symbolic links.

# Given a variable name, filename and variable name description, produce a rule
# ensuring the existence of a file in a cached location depending on the
# availability of the file in an indicated location or a previously used "old"
# location.

# Usage:
# $(eval $(call referenced_data_template,DATA_DIR,FILENAME,DESCRIPTION))

# Output summary:
# $(DATA_DIR_CACHED)/FILENAME: ...
# 	Link to $(DATA_DIR)/FILENAME from $(DATA_DIR_CACHED)/FILENAME
# 	or link to $(DATA_DIR_OLD)/FILENAME from $(DATA_DIR_CACHED)/FILENAME
# 	or raise an error



define referenced_data_template =
$$($(1)_CACHED)/$(2): | $$($(1)_CACHED)
	@if [ -e "$$($(1))" ] ; then \
		if [ -e "$$($(1))/$(2)" ] ; then \
			$(LINK) `realpath $$($(1))/$(2)` $$($(1)_CACHED)/$(2) ; \
		else \
			echo 1>&2 ; \
			echo "File $(2) is missing from $$($(1))." 1>&2 ; \
			echo 1>&2 ; \
			exit 1 ; \
		fi \
	elif [ -e "$$($(1)_OLD)" ] ; then \
		if [ -e "$$($(1)_OLD)/$(2)" ] ; then \
			$(LINK) `realpath $$($(1)_OLD)/$(2)` $$($(1)_CACHED)/$(2) ; \
		else \
			echo 1>&2 ; \
			echo "File $(2) is missing from $$($(1)_OLD)." 1>&2 ; \
			echo 1>&2 ; \
			exit 1 ; \
		fi \
	else \
		echo 1>&2 ; \
		echo "Please specify $(1) indicating $(3)." 1>&2 ; \
		echo 1>&2 ; \
		exit 1 ; \
	fi
endef
