# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# A convenience template for ensuring directory presence.

# Usage:
# $(eval $(call directory_template,DIRECTORY))

# Examples:
# $(eval $(call directory_template,$(METADATA)))



define directory_template =
$(1):
	mkdir -p $(1)
endef
