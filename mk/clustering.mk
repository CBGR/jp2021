# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Unpack and process clustering data for a given JASPAR release.

# Usage:
# $(eval $(call clustering_template,RELEASE))



define clustering_template =

# Input data.

JASPAR_CLUSTERING_DATA_$(1)		= $(JASPAR_DATA_DIR_CACHED)/jaspar-clustering-data-$(1).tar

# Output directories.

CLUSTERING_DATA_$(1)			= $(CLUSTERING_DATA)/$(1)
INCOMING_CLUSTERING_DATA_$(1)		= $(INCOMING_CLUSTERING_DATA)/$(1)

# Marker files indicating availability of processed data.

CLUSTERING_DATA_MARKER_$(1)		= $(STATUS)/_clustering_.$(1)
INCOMING_CLUSTERING_DATA_MARKER_$(1)	= $(STATUS)/_clustering_incoming_.$(1)



# Unpack archived clustering data for further processing.

$$(INCOMING_CLUSTERING_DATA_MARKER_$(1)): $$(JASPAR_CLUSTERING_DATA_$(1)) | $$(INCOMING_CLUSTERING_DATA_$(1))
	$(TOOLS)/tar_extract $$(JASPAR_CLUSTERING_DATA_$(1)) $$(INCOMING_CLUSTERING_DATA_$(1)) && \
	touch $$@

# Restructure clustering data, fixing image details.

$$(CLUSTERING_DATA_MARKER_$(1)): $$(INCOMING_CLUSTERING_DATA_MARKER_$(1)) | $$(CLUSTERING_DATA_$(1))
	$(DATATOOLS)/restructure_clustering_data $$(INCOMING_CLUSTERING_DATA_$(1)) $$(CLUSTERING_DATA_$(1)) && \
	$(DATATOOLS)/fix_clustering_img $$(CLUSTERING_DATA_$(1)) && \
	touch $$@

endef
