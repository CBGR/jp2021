# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Produce a database archive for a given JASPAR release.

# Usage:
# $(eval $(call archives_database_template,RELEASE))

# Examples:
# $(eval $(call archives_database_template,2022))
# $(eval $(call archives_database_template,$(JASPAR_RELEASE_NEW)))



define archives_database_template =

# Database inputs.

DATABASE_$(1)			= $(ALL_DATABASES)/JASPAR$(1).sqlite3

# Release-specific output archives.

ARCHIVE_DATABASE_$(1)		= $(WORK)/JASPAR$(1).sql.gz

# Populated database indicator.

METADATA_UPDATED_MARKER_$(1)	= $(STATUS)/_metadata_updated_.$(1)



# Make the database archive.

$$(ARCHIVE_DATABASE_$(1)): $$(METADATA_UPDATED_MARKER_$(1))
	sqlite3 $$(DATABASE_$(1)) '.dump' | gzip -c > $$@

endef
