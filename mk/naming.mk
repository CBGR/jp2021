# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Download data to be used to produce synonym-related data.

# Remote sources.

GENE_INFO_URL		= ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz
UNIPROT_SPROT_URL	= ftp://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz



# Download data or use existing downloads. Existing downloads will be referenced
# using symbolic links and will therefore not be reinstalled.

$(GENE_INFO_CACHED): | $(JASPAR_DATA_DIR_CACHED)
	@if [ -e "$(GENE_INFO)" ] ; then \
		$(LINK) `realpath $(GENE_INFO)` $(GENE_INFO_CACHED) ; \
	elif [ -e $(GENE_INFO_OLD) ] ; then \
		$(LINK) `realpath $(GENE_INFO_OLD)` $(GENE_INFO_CACHED) ; \
	else wget -nv -P $(JASPAR_DATA_DIR_CACHED) $(GENE_INFO_URL) ; \
	fi

$(UNIPROT_SPROT_CACHED): | $(JASPAR_DATA_DIR_CACHED)
	@if [ -e "$(UNIPROT_SPROT)" ] ; then \
		$(LINK) `realpath $(UNIPROT_SPROT)` $(UNIPROT_SPROT_CACHED) ; \
	elif [ -e $(UNIPROT_SPROT_OLD) ] ; then \
		$(LINK) `realpath $(UNIPROT_SPROT_OLD)` $(UNIPROT_SPROT_CACHED) ; \
	else wget -nv -P $(JASPAR_DATA_DIR_CACHED) $(UNIPROT_SPROT_URL) ; \
	fi



# Prepare a synonyms table from the gene_info data.

$(SYNONYM_TABLE): $(GENE_INFO_CACHED) | $(NAMING_DATA)
	gunzip -c $(GENE_INFO_CACHED) | $(DATATOOLS)/make_synonyms > $(SYNONYM_TABLE)

# Prepare UniProt details.

$(UNIPROT_ACCESSIONS): $(UNIPROT_SPROT_CACHED) | $(NAMING_DATA)
	gunzip -c $(UNIPROT_SPROT_CACHED) | $(DATATOOLS)/parse_uniprot_accessions > $(UNIPROT_ACCESSIONS)

$(UNIPROT_GENES): $(UNIPROT_SPROT_CACHED) | $(NAMING_DATA)
	gunzip -c $(UNIPROT_SPROT_CACHED) | $(DATATOOLS)/parse_uniprot_genes > $(UNIPROT_GENES)
