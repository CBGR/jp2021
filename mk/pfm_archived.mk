# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Unpack archived PFM data, augmenting newly archived data from a particular
# release.

# Archived PFM data is used to generate sequence logos. The archived metadata is
# combined with imported data from the latest release to produce a complete
# record of archived PFM data.

# Usage:
# $(eval $(call pfm_archived_template,RELEASE))



define pfm_archived_template =

# Input data is taken from existing and generated archived profiles and from
# updated profiles.

ARCHIVED_DATA_$(1)		= $(ALL_DATA)/$(1)_archived
UPDATED_DATA_$(1)		= $(ALL_DATA)/$(1)

# Archived profiles are consolidated and logos are generated with the combined
# archived and updated profile data, since a complete record of profiles is
# needed for the logos.

ARCHIVED_PFM_DATA_$(1)		= $$(ARCHIVED_DATA_$(1))/pfm
LOGOS_$(1)			= $(LOGOS)/$(1)
UPDATED_PFM_DATA_$(1)		= $$(UPDATED_DATA_$(1))/pfm

# Archives.

ARCHIVE_ARCHIVED_PROFILES_$(1)	= $(WORK)/matrix_archived_$(1).tar.gz
ARCHIVE_LOGOS_$(1)		= $(WORK)/matrix_logos_$(1).tar.gz

# Marker file indicating the availability of existing archived data.

ARCHIVED_PFM_DATA_MARKER_$(1)	= $(STATUS)/_pfm_.$(1)_archived_new
LOGOS_MARKER_$(1)		= $(STATUS)/_logos_.$(1)
UNPACKED_PFM_DATA_MARKER_$(1)	= $(STATUS)/_pfm_.$(1)_archived
UPDATED_PFM_DATA_MARKER_$(1)	= $(STATUS)/_pfm_.$(1)



# Depend on generated archived metadata, exported from the new release by the
# pfm_template in pfm.mk.

$$(ARCHIVED_PFM_DATA_MARKER_$(1)): $(JASPAR_ARCHIVED_DATA_CACHED) $$(UNPACKED_PFM_DATA_MARKER_$(1)) | $$(ARCHIVED_PFM_DATA_$(1))
	( $(TOOLS)/tar_extract $(JASPAR_ARCHIVED_DATA_CACHED) $$(ARCHIVED_PFM_DATA_$(1)) ) && \
	touch $$@



# Produce logos.

$$(LOGOS_MARKER_$(1)): $$(ARCHIVED_PFM_DATA_MARKER_$(1)) $$(UPDATED_PFM_DATA_MARKER_$(1)) | $$(LOGOS_$(1))
	$(TOOLS)/create_logos.sh $$(ARCHIVED_PFM_DATA_$(1)) $$(UPDATED_PFM_DATA_$(1)) $$(LOGOS_$(1)) $(PROCESSES) && \
	touch $$@



# Archive PFM-related data.

$$(ARCHIVE_ARCHIVED_PROFILES_$(1)): $$(ARCHIVED_PFM_DATA_MARKER_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_ARCHIVED_PROFILES_$(1)) $$(ARCHIVED_PFM_DATA_$(1))

$$(ARCHIVE_LOGOS_$(1)): $$(LOGOS_MARKER_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_LOGOS_$(1)) $$(LOGOS_$(1))

endef
