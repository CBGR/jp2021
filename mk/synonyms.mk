# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Process naming-related data, producing synonym data for deployment.
# See naming.mk for the retrieval of the input data.

# Usage:
# $(eval $(call synonyms_template,RELEASE))

# Examples:
# $(eval $(call synonyms_template,2022))
# $(eval $(call synonyms_template,$(RELEASE)))



define synonyms_template =

# Resources for working and output.

SYNONYMS_$(1)			= $(SYNONYMS)/$(1)
SYNONYMS_WORK_$(1)		= $(SYNONYMS_WORK)/$(1)
SYNONYMS_DATABASE_$(1)		= $$(SYNONYMS_WORK_$(1))/synonyms.sqlite

# Inputs from JASPAR metadata.

MATRIX_PROFILES_$(1)		= $(METADATA)/$(1)/matrix_profiles
MATRIX_PROTEINS_$(1)		= $(METADATA)/$(1)/matrix_proteins

# Input products derived from the JASPAR metadata.

SYNONYM_PROFILES_$(1)		= $$(SYNONYMS_WORK_$(1))/profiles.jaspar
SYNONYM_PROTEINS_$(1)		= $$(SYNONYMS_WORK_$(1))/proteins.jaspar

# Outputs for use by other software.

SYNONYM_GENES_$(1)		= $$(SYNONYMS_WORK_$(1))/genes.jaspar
SYNONYM_PROFILE_GENES_$(1)	= $$(SYNONYMS_WORK_$(1))/profile_genes.jaspar
SYNONYM_LIST_$(1)		= $$(SYNONYMS_WORK_$(1))/synonyms.jaspar
SYNONYM_PROFILE_SUMMARY_$(1)	= $$(SYNONYMS_WORK_$(1))/profile_summary.jaspar

# All inputs to the actual synonym generation process.

SYNONYM_INPUTS_$(1)		= $$(SYNONYM_PROFILES_$(1)) $$(SYNONYM_PROTEINS_$(1)) \
				  $(SYNONYM_TABLE) $(UNIPROT_ACCESSIONS) $(UNIPROT_GENES)

# Outputs for installation including gene/protein source data.

SYNONYMS_PREPARED_$(1)		= $$(SYNONYM_GENES_$(1)) $$(SYNONYM_PROFILE_GENES_$(1)) \
				  $$(SYNONYM_LIST_$(1)) $$(SYNONYM_PROFILE_SUMMARY_$(1))
SYNONYMS_EXPORTED_$(1)		= $$(SYNONYMS_PREPARED_$(1)) $$(SYNONYM_PROFILES_$(1))

# Marker files for grouped targets.

SYNONYMS_EXPORTED_MARKER_$(1)	= $(STATUS)/_synonyms_exported_.$(1)
SYNONYMS_PREPARED_MARKER_$(1)	= $(STATUS)/_synonyms_prepared_.$(1)



# Extract JASPAR data.

$$(SYNONYM_PROFILES_$(1)): $$(MATRIX_PROFILES_$(1)) | $$(SYNONYMS_WORK_$(1))
	$(DATATOOLS)/synonym_profiles $$(MATRIX_PROFILES_$(1)) > $$(SYNONYM_PROFILES_$(1))

$$(SYNONYM_PROTEINS_$(1)): $$(MATRIX_PROTEINS_$(1)) | $$(SYNONYMS_WORK_$(1))
	cp $$(MATRIX_PROTEINS_$(1)) $$(SYNONYM_PROTEINS_$(1))



# Prepare and export data.

# Use the indicated method to prepare the database required for the eventually
# exported data. Then, export the results from the database if one is involved.
# Also prepare a more convenient summary of synonyms for profiles.

$$(eval $$(call grouped_target,$$(SYNONYMS_PREPARED_$(1)),$$(SYNONYMS_PREPARED_MARKER_$(1))))

$$(SYNONYMS_PREPARED_MARKER_$(1)): $$(SYNONYM_INPUTS_$(1)) | $$(SYNONYMS_WORK_$(1))
	$(DATATOOLS)/prepare_synonyms $$(SYNONYMS_DATABASE_$(1)) $$(SYNONYMS_WORK_$(1)) $(NAMING_DATA) && \
	$(DATATOOLS)/export_prepared_synonyms $$(SYNONYMS_DATABASE_$(1)) $$(SYNONYMS_WORK_$(1)) && \
	$(DATATOOLS)/profile_summary $$(SYNONYMS_WORK_$(1)) > $$(SYNONYM_PROFILE_SUMMARY_$(1)) && \
	touch $$@

$$(SYNONYMS_EXPORTED_MARKER_$(1)): $$(SYNONYMS_EXPORTED_$(1)) | $$(SYNONYMS_$(1))
	cp $$(SYNONYMS_EXPORTED_$(1)) $$(SYNONYMS_$(1)) && \
	touch $$@

endef
