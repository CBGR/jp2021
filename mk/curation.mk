# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Curation data processing for both updated and revised curation data.
# The programs employed in this file perform the main metadata processing
# activity, producing the metadata for the new (and revised) release, using
# incoming and existing metadata.

# Process curation data.

$(UPDATED_METADATA_MARKER): $(EXISTING_MATRIX_DETAILS) $(EXISTING_MATRIX_FILES) \
			    $(EXISTING_MATRIX_TFFM) \
			    $(INCOMING_DELETION_DATA) $(INCOMING_MATRIX_DETAILS) \
			    $(JASPAR_MATRIX_HISTORY_CACHED) \
			    $(TAX_GROUP_MEMBERS) $(TAX_DETAILS) \
			    $(DATA)/tax_groups
	$(DATATOOLS)/process_curation_data $(DATABASE) $(JASPAR_MATRIX_HISTORY_CACHED) \
					   $(EXISTING_METADATA) $(EXISTING_METADATA_TFFM) \
					   $(INCOMING_METADATA) $(TAXONOMY_DATA) && \
	touch $@

# Process revised curation data.

$(UPDATED_METADATA_REVISED_MARKER): $(INCOMING_DELETION_REVISION_DATA) \
				    $(INCOMING_MATRIX_DETAILS_REVISED) \
				    $(UPDATED_METADATA_MARKER)
	cp $(DATABASE) $(DATABASE_REVISED) && \
	$(DATATOOLS)/process_curation_revision_data $(DATABASE_REVISED) \
						    $(INCOMING_METADATA_REVISED) && \
	touch $@
