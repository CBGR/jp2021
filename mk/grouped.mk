# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# A template providing rules for a group of files, making each file dependent on
# a common marker file. When all files have been updated by the appropriate
# rule, the marker file should also be created or updated, thus preventing
# attempts to update the group's files again.

# Usage:
# $(eval $(call grouped_target,FILES,MARKER_FILE))

# Example:
#
# $(eval $(call grouped_target,$(NEW_FILES),$(NEW_FILES_MARKER)))
#
# $(NEW_FILES_MARKER) : $(OLD_FILES)
# 	process $(OLD_FILES) $(NEW_FILES)
# 	touch $@



define grouped_target =

# Make each file depend individually on the marker file. With the marker file
# representing the collection of files, this permits each file to be a
# representative of the complete collection.

# When the marker file has been updated, each file is made newer than the marker
# file to prevent an unsatisfied dependency from being reported (by make's -n
# option) even if no attempt to satisfy the dependency would actually occur in a
# genuine invocation of make.

$(1): $(2)
	touch $$@

# Here, any missing file from the group will cause the marker file to be
# removed, thus initiating an update of the entire group.

$(foreach FILE,$(1),$(if $(wildcard $(FILE)),,$(shell rm -f $(2))))

endef
