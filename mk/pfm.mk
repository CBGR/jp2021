# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Produce PFM data for a given JASPAR release.

# Usage:
# $(eval $(call pfm_template,RELEASE))



define pfm_template =

MATRIX_DATA_LABELLED_$(1)	= $(METADATA)/$(1)/matrix_data_labelled
PFM_DATA_$(1)			= $(ALL_DATA)/$(1)/pfm
PFM_DATA_MARKER_$(1)		= $(STATUS)/_pfm_.$(1)

$$(PFM_DATA_MARKER_$(1)): $$(MATRIX_DATA_LABELLED_$(1)) | $$(PFM_DATA_$(1)) $(STATUS)
	( $(TOOLS)/tab2pfm < $$(MATRIX_DATA_LABELLED_$(1)) | $(TOOLS)/splitpfms $$(PFM_DATA_$(1)) ) && \
	touch $$@
endef
