# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Produce clustering data archive for a given JASPAR release.

# Usage:
# $(eval $(call archives_clustering_template,RELEASE))



define archives_clustering_template =

# Release-specific output archive.

ARCHIVE_CLUSTERING_DATA_$(1)	= $(WORK)/matrix_clustering_$(1).tar.gz

# Output directory.

CLUSTERING_DATA_$(1)		= $(CLUSTERING_DATA)/$(1)

# Marker file indicating availability of generated data.

CLUSTERING_DATA_MARKER_$(1)	= $(STATUS)/_clustering_.$(1)



# Make clustering archives.

$$(ARCHIVE_CLUSTERING_DATA_$(1)): $$(CLUSTERING_DATA_MARKER_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_CLUSTERING_DATA_$(1)) $$(CLUSTERING_DATA_$(1))

endef
