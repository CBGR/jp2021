-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Assign profile identifier numbering.

delete from profile_identifier_seqnums;

insert into profile_identifier_seqnums
    (id)
    select id
    from matrix_updated
    where base_id like 'MA?%';

analyze profile_identifier_seqnums;

delete from unvalidated_profile_identifier_seqnums;

insert into unvalidated_profile_identifier_seqnums
    (id)
    select id
    from matrix_updated
    where base_id like 'UN?%';

analyze unvalidated_profile_identifier_seqnums;

-- Make the assignments in the data using the numbering initialised above.

update matrix_updated
    set base_id = 'MA' || (select substr('0000' || seqnum, -4) from profile_identifier_seqnums where id = matrix_updated.id)
    where id in (select id from profile_identifier_seqnums);

update matrix_updated
    set base_id = 'UN' || (select substr('0000' || seqnum, -4) from unvalidated_profile_identifier_seqnums where id = matrix_updated.id)
    where id in (select id from unvalidated_profile_identifier_seqnums);

-- vim: tabstop=4 expandtab shiftwidth=4
