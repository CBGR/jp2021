-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

delete from matrix_updated;
delete from matrix_data_updated;
delete from matrix_species_updated;
delete from matrix_annotation_updated;
delete from matrix_protein_updated;
delete from matrix_file_details_updated;
delete from matrix_history_updated;

-- vim: tabstop=4 expandtab shiftwidth=4
