-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Process existing matrix profile records.

-- Fix existing annotations.

update matrix_annotation set val = trim(val, ' ');

-- Update table statistics.

create index matrix_base_id_version on matrix (base_id, version);
create index matrix_history_base_id on matrix_history (base_id);

analyze matrix;
analyze matrix_data;
analyze matrix_history;
analyze matrix_species;
analyze matrix_annotation;
analyze matrix_protein;
analyze matrix_file_details;

-- vim: tabstop=4 expandtab shiftwidth=4
