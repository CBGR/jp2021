-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Remove obsolete collections from the new data.

create temporary table tmp_obsolete as
    select id, base_id
    from matrix_updated
    where collection not in ('CORE', 'UNVALIDATED');

-- Copy obsolete profile details to the archived tables.

insert into matrix_archived
    select id,
           collection,
           base_id,
           version,
           name
    from matrix_updated
    where id in (select id from tmp_obsolete);

insert into matrix_data_archived
    select matrix_data_updated.id,
           matrix_data_updated.row,
           matrix_data_updated.col,
           matrix_data_updated.val
    from matrix_data_updated
    inner join tmp_obsolete
        on matrix_data_updated.id = tmp_obsolete.id;

insert into matrix_species_archived
    select matrix_species_updated.id,
           matrix_species_updated.tax_id
    from matrix_species_updated
    inner join tmp_obsolete
        on matrix_species_updated.id = tmp_obsolete.id;

insert into matrix_annotation_archived
    select matrix_annotation_updated.id,
           matrix_annotation_updated.tag,
           matrix_annotation_updated.val
    from matrix_annotation_updated
    inner join tmp_obsolete
        on matrix_annotation_updated.id = tmp_obsolete.id;

insert into matrix_protein_archived
    select matrix_protein_updated.id,
           matrix_protein_updated.acc
    from matrix_protein_updated
    inner join tmp_obsolete
        on matrix_protein_updated.id = tmp_obsolete.id;

-- Delete the obsolete profile details.

delete from matrix_updated
where id in (select id from tmp_obsolete);

delete from matrix_annotation_updated
where id in (select id from tmp_obsolete);

delete from matrix_data_updated
where id in (select id from tmp_obsolete);

delete from matrix_protein_updated
where id in (select id from tmp_obsolete);

delete from matrix_species_updated
where id in (select id from tmp_obsolete);

delete from matrix_file_details_updated
where base_id in (
    select base_id from tmp_obsolete);

-- vim: tabstop=4 expandtab shiftwidth=4
