-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Unchanged, new and updated profiles with associated data and metadata.

create table matrix_updated (
    id integer not null,
    collection varchar(16) not null,
    base_id varchar(16) not null,
    version integer not null default 1,
    name varchar(255) not null,
    primary key (id autoincrement)
);

create table matrix_data_updated (
    id integer not null,
    row varchar(1) not null,
    col integer not null,
    val float(10,3) not null,
    primary key (id, row, col)
);

create table matrix_history_updated (
    collection varchar(16) not null,
    base_id varchar(16) not null,
    version integer not null,
    name varchar(255) not null,
    release varchar(16) not null,
    primary key (base_id, release)
);

create table matrix_species_updated (
    id integer not null,
    tax_id integer not null,
    primary key (id, tax_id)
);

create table matrix_annotation_updated (
    id integer not null,
    tag varchar(255) not null,
    val varchar(255) not null
);

create table matrix_protein_updated (
    id integer not null,
    acc varchar(255) not null,
    primary key (id, acc)
);

-- Updated file details.

create table matrix_file_details_updated (
    filetype varchar(16) not null,
    base_id varchar(16) not null,
    version integer not null,
    filename varchar(255) not null,
    primary key (filetype, base_id, version)
);

-- vim: tabstop=4 expandtab shiftwidth=4
