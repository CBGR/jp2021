-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Remove deleted profiles. Replaced profiles will already be excluded from the
-- updated data, so this removes unreplaced profiles.

-- Use the updated table to obtain identifiers since replacement profiles will
-- have reused identifiers from the profiles they replaced, but their profile
-- details will be different.

create temporary table tmp_deleted_from_updated as
    select id
    from matrix_updated
    inner join matrix_deletion_incoming
        on base_id || '.' || version = matrix_id;

delete from matrix_updated
where id in (select id from tmp_deleted_from_updated);

delete from matrix_data_updated
where id in (select id from tmp_deleted_from_updated);

delete from matrix_species_updated
where id in (select id from tmp_deleted_from_updated);

delete from matrix_annotation_updated
where id in (select id from tmp_deleted_from_updated);

delete from matrix_protein_updated
where id in (select id from tmp_deleted_from_updated);



-- Archive deleted profile details.

create temporary table tmp_deleted_from_existing as
    select matrix.id
    from matrix
    inner join matrix_deletion_incoming
        on base_id || '.' || version = matrix_id
    left outer join existing_identifier_mapping
        on matrix.id = existing_identifier_mapping.id
        and action in ('A', 'V')
    where existing_identifier_mapping.id is null;

insert into matrix_archived
    select id,
           collection,
           base_id,
           version,
           name
    from matrix
    where id in (select id from tmp_deleted_from_existing);

insert into matrix_data_archived
    select matrix_data.id,
           matrix_data.row,
           matrix_data.col,
           matrix_data.val
    from matrix_data
    inner join tmp_deleted_from_existing
        on matrix_data.id = tmp_deleted_from_existing.id;

insert into matrix_species_archived
    select matrix_species.id,
           matrix_species.tax_id
    from matrix_species
    inner join tmp_deleted_from_existing
        on matrix_species.id = tmp_deleted_from_existing.id;

insert into matrix_annotation_archived
    select matrix_annotation.id,
           matrix_annotation.tag,
           matrix_annotation.val
    from matrix_annotation
    inner join tmp_deleted_from_existing
        on matrix_annotation.id = tmp_deleted_from_existing.id;

insert into matrix_protein_archived
    select matrix_protein.id,
           matrix_protein.acc
    from matrix_protein
    inner join tmp_deleted_from_existing
        on matrix_protein.id = tmp_deleted_from_existing.id;

-- vim: tabstop=4 expandtab shiftwidth=4
