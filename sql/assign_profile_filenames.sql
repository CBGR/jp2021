-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Aggregate the incoming file details for new profiles, adding profile
-- identifiers.

insert into matrix_file_details_updated
    (filetype, base_id, version, filename)
    select 'bed', base_id, version, bed_file
    from matrix_files_incoming as F
    inner join new_identifier_mapping as N
        on F.id = N.arbitrary_id
    inner join matrix_updated as M
        on N.id = M.id
    where action in ('A', 'N')
        and bed_file is not null
    union all
    select 'fasta', base_id, version, fasta_file
    from matrix_files_incoming as F
    inner join new_identifier_mapping as N
        on F.id = N.arbitrary_id
    inner join matrix_updated as M
        on N.id = M.id
    where action in ('A', 'N')
        and fasta_file is not null
    union all
    select 'centrality', base_id, version, centrality_file
    from matrix_files_incoming as F
    inner join new_identifier_mapping as N
        on F.id = N.arbitrary_id
    inner join matrix_updated as M
        on N.id = M.id
    where action in ('A', 'N')
        and centrality_file is not null;

-- vim: tabstop=4 expandtab shiftwidth=4
