-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Introduce annotations for taxonomy groups.

insert into matrix_annotation_updated
    select distinct
           matrix_annotation_updated.id,
           'tax_group' as tag,
           tax_groups.common_name as val
    from matrix_annotation_updated
    inner join matrix_species_updated
        on matrix_annotation_updated.id = matrix_species_updated.id
    inner join tax_group_members
        on matrix_species_updated.tax_id = tax_group_members.tax_id
    inner join tax_groups
        on tax_group_members.group_id = tax_groups.tax_id
    where matrix_annotation_updated.id not in (
        select id from matrix_annotation_updated where tag = 'tax_group');

-- vim: tabstop=4 expandtab shiftwidth=4
