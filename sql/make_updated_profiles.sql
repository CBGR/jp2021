-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Obtain updated profiles with 'U' actions. Such profiles are treated as new
-- records having new identifiers, but their base identifiers are shared with
-- previous versions, and their versions are incremented from previous versions.
-- (The incoming data employs the previous version.)

-- Determine the updated profiles, handling revised data that has already been
-- incorporated into the curation data.

create temporary table tmp_updated as
    select new_identifier_mapping.id as id,
           new_identifier_mapping.arbitrary_id as arbitrary_id,
           matrix_incoming.base_id as base_id,
           matrix_incoming.version + 1 as version,
           matrix_incoming.name as name
    from matrix_incoming
    inner join new_identifier_mapping
        on matrix_incoming.id = new_identifier_mapping.arbitrary_id
    left outer join matrix_updated
        on matrix_incoming.base_id = matrix_updated.base_id
        and matrix_incoming.version + 1 = matrix_updated.version
    where action = 'U'
        and matrix_updated.base_id is null;

insert into matrix_updated
    select id,
           case substr(base_id, 1, 2)
               when 'MA' then 'CORE'
               when 'UN' then 'UNVALIDATED'
           end as collection,
           base_id,
           version,
           name
    from tmp_updated;

-- Obtain matrix data for updated profiles from the incoming data files.

insert into matrix_data_updated
    select tmp_updated.id,
           matrix_data_incoming.row,
           matrix_data_incoming.col,
           matrix_data_incoming.val
    from matrix_data_incoming
    inner join tmp_updated
        on matrix_data_incoming.id = arbitrary_id;

-- Obtain species data for updated profiles from the incoming data files.

insert into matrix_species_updated
    select tmp_updated.id,
           matrix_species_incoming.tax_id
    from matrix_species_incoming
    inner join tmp_updated
        on matrix_species_incoming.id = arbitrary_id;

-- Obtain annotation data for updated profiles excluding comments since these
-- are curation-related comments in the incoming curation data.

insert into matrix_annotation_updated
    select tmp_updated.id,
           matrix_annotation_incoming.tag,
           matrix_annotation_incoming.val
    from matrix_annotation_incoming
    inner join tmp_updated
        on matrix_annotation_incoming.id = arbitrary_id
    where tag <> 'comment';

-- Propagate validation details from existing profile versions.
-- Note that "Validation" may appear in the incoming data, being a synonym for
-- the established "medline" annotation.

insert into matrix_annotation_updated
    select tmp_updated.id,
           matrix_annotation.tag,
           matrix_annotation.val
    from matrix_incoming
    inner join tmp_updated
        on matrix_incoming.id = arbitrary_id
    inner join matrix
        on matrix_incoming.base_id = matrix.base_id
        and matrix_incoming.version = matrix.version
    inner join matrix_annotation
        on matrix.id = matrix_annotation.id
    where tag = 'medline';

-- Obtain protein data for updated profiles from the incoming data files.

insert into matrix_protein_updated
    select tmp_updated.id,
           matrix_protein_incoming.acc
    from matrix_protein_incoming
    inner join tmp_updated
        on matrix_protein_incoming.id = arbitrary_id;

-- Aggregate the incoming file details for updated profiles.

insert into matrix_file_details_updated
    (filetype, base_id, version, filename)
    select 'bed', M.base_id, M.version + 1, bed_file
    from matrix_files_incoming as F
    inner join tmp_updated as N
        on F.id = N.arbitrary_id
    inner join matrix_incoming as M
        on F.id = M.id
    where bed_file is not null
    union all
    select 'fasta', M.base_id, M.version + 1, fasta_file
    from matrix_files_incoming as F
    inner join tmp_updated as N
        on F.id = N.arbitrary_id
    inner join matrix_incoming as M
        on F.id = M.id
    where fasta_file is not null
    union all
    select 'centrality', M.base_id, M.version + 1, centrality_file
    from matrix_files_incoming as F
    inner join tmp_updated as N
        on F.id = N.arbitrary_id
    inner join matrix_incoming as M
        on F.id = M.id
    where centrality_file is not null;

-- vim: tabstop=4 expandtab shiftwidth=4
