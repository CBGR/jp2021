-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Obtain sequence numbers for the different identifier categories.

delete from base_id_seqnums;

insert into base_id_seqnums
    select prefix, max(seqnum) + 1 as seqnum
    from (
        select substr(base_id, 1, 2) as prefix,
               cast(substr(base_id, 3) as integer) as seqnum
        from matrix)
    group by prefix;

-- Hack to assign sequence numbers for profile numbering.

delete from sqlite_sequence where name = 'profile_identifier_seqnums';

insert into sqlite_sequence
    (name, seq)
    select 'profile_identifier_seqnums', seqnum
    from base_id_seqnums
    where prefix = 'MA';

delete from sqlite_sequence where name = 'unvalidated_profile_identifier_seqnums';

insert into sqlite_sequence
    (name, seq)
    select 'unvalidated_profile_identifier_seqnums', seqnum
    from base_id_seqnums
    where prefix = 'UN';

-- vim: tabstop=4 expandtab shiftwidth=4
