-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Database table definitions for the different kinds of matrix profile records,
-- separated into existing, incoming, updated and new categories.



-- Existing records.
-- See: init_curation_existing_preparation.sql

create table tffm (
    id integer not null,
    base_id varchar(16) not null,
    version integer not null,
    matrix_base_id varchar(16) not null,
    matrix_version integer not null,
    name varchar(255) not null,
    log_p_1st_order float default null,
    log_p_detailed float default null,
    experiment_name varchar(255) not null,
    primary key (id)
);



-- Incoming records to be combined with existing ones.
-- See: init_curation_incoming_preparation.sql

create table tffm_incoming (
    matrix_id varchar (16) not null,
    dataset varchar(255) not null,
    experiment_name varchar(255) not null,
    log_p_1st_order float default null,
    log_p_detailed float default null,
    primary key (matrix_id)
);



-- Unchanged, new and updated profiles with associated data and metadata.
-- See: init_curation_updated_preparation.sql

create table tffm_updated (
    id integer not null,
    base_id varchar(16) not null,
    version integer not null,
    matrix_base_id varchar(16) not null,
    matrix_version integer not null,
    name varchar(255) not null,
    log_p_1st_order float default null,
    log_p_detailed float default null,
    experiment_name varchar(255) not null,
    primary key (id autoincrement)
);



-- Updated TFFM profile mapping.

create table tffm_file_details_updated (
    matrix_id varchar(16) not null,
    dataset varchar(255) not null,
    tffm_id varchar(16) not null,
    primary key (tffm_id)
);



-- Archived records not propagated to the updated table.

create table matrix_archived (
    id integer not null,
    collection varchar(16) not null,
    base_id varchar(16) not null,
    version integer not null default 1,
    name varchar(255) not null,
    primary key (id autoincrement)
);

create table matrix_data_archived (
    id integer not null,
    row varchar(1) not null,
    col integer not null,
    val float(10,3) not null,
    primary key (id, row, col)
);

create table matrix_history_archived (
    collection varchar(16) not null,
    base_id varchar(16) not null,
    version integer not null,
    name varchar(255) not null,
    release varchar(16) not null,
    primary key (base_id, release)
);

create table matrix_species_archived (
    id integer not null,
    tax_id integer not null,
    primary key (id, tax_id)
);

create table matrix_annotation_archived (
    id integer not null,
    tag varchar(255) not null,
    val varchar(255) not null
);

create table matrix_protein_archived (
    id integer not null,
    acc varchar(255) not null,
    primary key (id, acc)
);



-- Mapping from arbitrary identifiers to existing identifiers.

create table existing_identifier_mapping (
    id integer not null,
    arbitrary_id integer not null,
    action varchar(16) not null,
    primary key (id autoincrement)
);

-- Mapping from arbitrary identifiers to newly assigned identifiers.

create table new_identifier_mapping (
    id integer not null,
    arbitrary_id integer not null,
    action varchar(16) not null,
    primary key (id autoincrement)
);



-- Tag synonyms to map to the existing tags.

create table tag_synonyms (
    tag varchar(255) not null,
    synonym varchar(255) not null,
    primary key (tag, synonym)
);



-- Taxonomy naming details for the updated data.

create table tax (
    tax_id integer not null,
    species varchar(255) not null,
    primary key (tax_id)
);

create table tax_ext (
    tax_id integer not null,
    name varchar(255) not null,
    primary key (tax_id)
);

-- Taxonomy group details.

create table tax_groups (
    tax_id integer not null,
    scientific_name varchar(255) not null,
    common_name varchar(255) not null,
    primary key (tax_id)
);

-- Taxonomy group membership details.

create table tax_group_members (
    group_id integer not null,
    tax_id integer not null,
    primary key (group_id, tax_id)
);



-- Sequence numbers for the different identifier categories.

create table base_id_seqnums (
    prefix char(2) not null,
    seqnum integer not null,
    primary key (prefix)
);

-- Mapping from record identifiers to profile identifier numbering.

create table profile_identifier_seqnums (
    id integer not null,
    seqnum integer not null,
    primary key (seqnum autoincrement)
);

create table unvalidated_profile_identifier_seqnums (
    id integer not null,
    seqnum integer not null,
    primary key (seqnum autoincrement)
);

create table tffm_identifier_seqnums (
    id integer not null,
    seqnum integer not null,
    primary key (seqnum autoincrement)
);

-- vim: tabstop=4 expandtab shiftwidth=4
