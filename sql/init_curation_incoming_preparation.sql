-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Incoming records to be combined with existing ones.

create table matrix_incoming (
    id integer not null,
    collection varchar(16) not null,
    base_id varchar(16),              -- optional, may not be assigned
    version integer,                  -- optional, may not be assigned
    name varchar(255) not null,
    primary key (id autoincrement)
);

create table matrix_action_incoming (
    id integer not null,
    action varchar(16),
    primary key (id)
);

create table matrix_data_incoming (
    id integer not null,
    row varchar(1) not null,
    col integer not null,
    val float(10,3) not null,
    primary key (id, row, col)
);

create table matrix_files_incoming (
    id integer not null,
    bed_file varchar(255),
    fasta_file varchar(255),
    centrality_file varchar(255),
    primary key (id autoincrement)
);

create table matrix_species_incoming (
    id integer not null,
    tax_id integer not null,
    primary key (id, tax_id)
);

create table matrix_annotation_incoming (
    id integer not null,
    tag varchar(255) not null,
    val varchar(255) not null
);

create table matrix_protein_incoming (
    id integer not null,
    acc varchar(255) not null,
    primary key (id, acc)
);

create table matrix_deletion_incoming (
    matrix_id varchar(16) not null,
    primary key (matrix_id)
);

-- vim: tabstop=4 expandtab shiftwidth=4
