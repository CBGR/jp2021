-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Aggregate the existing history.

insert into matrix_history_updated
    select collection, base_id, version, name, release
    from matrix_history;

-- vim: tabstop=4 expandtab shiftwidth=4
