-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Hack to assign sequence numbers, setting the identifiers used in the mapping
-- to follow on from those in the existing matrix table.

delete from sqlite_sequence where name = 'new_identifier_mapping';

insert into sqlite_sequence
    (name, seq)
    select 'new_identifier_mapping', max(id)
    from matrix;

-- Create a mapping from arbitrary identifiers used by incoming records to new
-- identifiers.
-- Omit the id to cause the autoincremented default to be used.
-- Restrict the selection of incoming profiles using the action, mostly to
-- filter out uncertain records.

delete from new_identifier_mapping;

insert into new_identifier_mapping
    (arbitrary_id, action)
    select matrix_incoming.id, action
    from matrix_incoming
    inner join matrix_action_incoming
        on matrix_incoming.id = matrix_action_incoming.id
    left outer join matrix
        on matrix_incoming.base_id = matrix.base_id
        and matrix_incoming.version = matrix.version
    where matrix.id is null
        and action in ('A', 'N');

-- New versions of existing profiles creating new identifiers (and records).

insert into new_identifier_mapping
    (arbitrary_id, action)
    select matrix_incoming.id, action
    from matrix_incoming
    inner join matrix
        on matrix_incoming.base_id = matrix.base_id
        and matrix_incoming.version = matrix.version
    inner join matrix_action_incoming
        on matrix_incoming.id = matrix_action_incoming.id
    where action = 'U';

create index new_identifier_mapping_arbitrary_id on new_identifier_mapping (arbitrary_id);

analyze new_identifier_mapping;

-- Create a mapping from arbitrary identifiers used by incoming records to
-- existing identifiers.
-- The superseded versions of profiles that are updated (with action 'U') are
-- excluded here. Such profiles will be propagated separately.
-- Restrict the selection of incoming profiles using the action, mostly to
-- filter out uncertain records.

delete from existing_identifier_mapping;

insert into existing_identifier_mapping
    (id, arbitrary_id, action)
    select matrix.id, matrix_incoming.id, action
    from matrix_incoming
    inner join matrix
        on matrix_incoming.base_id = matrix.base_id
        and matrix_incoming.version = matrix.version
    inner join matrix_action_incoming
        on matrix_incoming.id = matrix_action_incoming.id
    where action in ('A', 'V');

create index existing_identifier_mapping_arbitrary_id on existing_identifier_mapping (arbitrary_id);

analyze existing_identifier_mapping;

-- vim: tabstop=4 expandtab shiftwidth=4
