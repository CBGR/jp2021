-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Obtain new profiles unrelated to any existing profiles (which will only be
-- categorised as assigned, 'A', or non-validated, 'N').

insert into matrix_updated
    select new_identifier_mapping.id,
           case action
               when 'A' then 'CORE'
               when 'N' then 'UNVALIDATED'
           end as collection,
           case action
               when 'A' then 'MA????'
               when 'N' then 'UN????'
           end as base_id,
           1 as version,
           matrix_incoming.name
    from matrix_incoming
    inner join new_identifier_mapping
        on matrix_incoming.id = new_identifier_mapping.arbitrary_id
    where action in ('A', 'N');

-- Obtain matrix data for new profiles from the incoming data files.

insert into matrix_data_updated
    select new_identifier_mapping.id,
           row,
           col,
           val
    from matrix_data_incoming
    inner join new_identifier_mapping
        on matrix_data_incoming.id = new_identifier_mapping.arbitrary_id
    where action in ('A', 'N');

-- Obtain species data for new profiles from the incoming data files.

insert into matrix_species_updated
    select new_identifier_mapping.id,
           tax_id
    from matrix_species_incoming
    inner join new_identifier_mapping
        on matrix_species_incoming.id = new_identifier_mapping.arbitrary_id
    where action in ('A', 'N');

-- Obtain annotation data for new profiles from the incoming data files.

insert into matrix_annotation_updated
    select new_identifier_mapping.id,
           tag,
           val
    from matrix_annotation_incoming
    inner join new_identifier_mapping
        on matrix_annotation_incoming.id = new_identifier_mapping.arbitrary_id
    where action in ('A', 'N');

-- Obtain protein data for new profiles from the incoming data files.

insert into matrix_protein_updated
    select new_identifier_mapping.id,
           acc
    from matrix_protein_incoming
    inner join new_identifier_mapping
        on matrix_protein_incoming.id = new_identifier_mapping.arbitrary_id
    where action in ('A', 'N');

-- vim: tabstop=4 expandtab shiftwidth=4
