-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Combine existing data into the matrix history.

insert into matrix_history_updated
    (collection, base_id, version, name, release)
    select collection, matrix.base_id, matrix.version, name, '2020'
    from matrix
    inner join (
        select base_id, max(version) as version
        from matrix
        group by base_id
        ) as latest
        on matrix.base_id = latest.base_id
        and matrix.version = latest.version;

-- vim: tabstop=4 expandtab shiftwidth=4
