-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Update TFFM details with new identifiers.

-- Copy the existing TFFM details first.

delete from tffm_updated;

insert into tffm_updated
select * from tffm;

-- Hack to assign sequence numbers for profile numbering.

-- Surrogate key numbering.

delete from sqlite_sequence where name = 'tffm_updated';

insert into sqlite_sequence
    (name, seq)
    select 'tffm_updated', max(id) + 1
    from tffm_updated;

-- TFFM base identifier numbering.

delete from sqlite_sequence where name = 'tffm_identifier_seqnums';

insert into sqlite_sequence
    (name, seq)
    select 'tffm_identifier_seqnums', max(cast(substr(base_id, 5) as integer)) + 1
    from tffm_updated;

-- Introduce new versions for recognised TFFMs.

insert into tffm_updated
    (base_id, version, matrix_base_id, matrix_version, name, log_p_1st_order, log_p_detailed, experiment_name)
    select
        T.base_id,
        T.version + 1,
        T.matrix_base_id,
        T.matrix_version,
        M.name,
        I.log_p_1st_order,
        I.log_p_detailed,
        I.experiment_name
    from tffm_incoming as I
    inner join matrix_updated as M
        on M.base_id = substr(matrix_id, 1, 6)
        and M.version = substr(matrix_id, 8)
    inner join tffm as T
        on T.matrix_base_id || '.' || T.matrix_version = I.matrix_id
    order by I.matrix_id;

-- Introduce new profiles.

insert into tffm_updated
    (base_id, version, matrix_base_id, matrix_version, name, log_p_1st_order, log_p_detailed, experiment_name)
    select
        'TFFM????',
        1,
        substr(I.matrix_id, 1, 6),
        substr(I.matrix_id, 8),
        M.name,
        I.log_p_1st_order,
        I.log_p_detailed,
        I.experiment_name
    from tffm_incoming as I
    inner join matrix_updated as M
        on M.base_id = substr(matrix_id, 1, 6)
        and M.version = substr(matrix_id, 8)
    left outer join tffm as T
        on T.matrix_base_id || '.' || T.matrix_version = I.matrix_id
    where T.id is null
    order by I.matrix_id;

-- Remove old versions for recognised TFFMs.

delete from tffm_updated
where id in (
    select U.id
    from tffm_updated as U
    inner join (
        select matrix_base_id, matrix_version, max(id) as latest
        from tffm_updated
        group by matrix_base_id, matrix_version
        having count(*) > 1
        ) as D
        on U.matrix_base_id = D.matrix_base_id
        and U.matrix_version = D.matrix_version
    where U.id <> D.latest);

-- Assign profile identifier numbering.

delete from tffm_identifier_seqnums;

insert into tffm_identifier_seqnums
    (id)
    select id
    from tffm_updated
    where base_id like 'TFFM?%';

analyze tffm_identifier_seqnums;

-- Make the assignments in the data using the numbering initialised above.

update tffm_updated
    set base_id = 'TFFM' || (select substr('0000' || seqnum, -4) from tffm_identifier_seqnums where id = tffm_updated.id)
    where id in (select id from tffm_identifier_seqnums);

-- Generate the basis of a mapping for the profiles so that files can be copied
-- to appropriate locations.

delete from tffm_file_details_updated;

insert into tffm_file_details_updated
    select matrix_base_id || '.' || matrix_version, dataset, base_id || '.' || version
    from tffm_updated as T
    inner join tffm_incoming as I
        on matrix_base_id || '.' || matrix_version = matrix_id;

-- vim: tabstop=4 expandtab shiftwidth=4
