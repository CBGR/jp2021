-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Process incoming curation records.

-- Fix nulls.

update matrix_incoming set base_id = null where base_id = '';
update matrix_incoming set version = null where version = '';
update matrix_action_incoming set action = null where action = '';
update matrix_files_incoming set bed_file = null where bed_file = '';
update matrix_files_incoming set centrality_file = null where centrality_file = '';
update matrix_files_incoming set fasta_file = null where fasta_file = '';

-- Fix incoming annotations.

update matrix_annotation_incoming set val = trim(val, ' ');

-- Rewrite annotations.

update matrix_annotation_incoming
    set tag = (select tag from tag_synonyms where synonym = matrix_annotation_incoming.tag)
    where tag not in (select tag from tag_synonyms);

-- Update table statistics.

create index matrix_incoming_base_id_version on matrix_incoming (base_id, version);

analyze matrix_incoming;
analyze matrix_action_incoming;
analyze matrix_data_incoming;
analyze matrix_files_incoming;
analyze matrix_species_incoming;
analyze matrix_annotation_incoming;
analyze matrix_protein_incoming;
analyze matrix_deletion_incoming;

-- vim: tabstop=4 expandtab shiftwidth=4
