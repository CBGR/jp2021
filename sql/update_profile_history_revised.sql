-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Remove old history entries for this release.

delete from matrix_history_updated where release = '2022';

-- vim: tabstop=4 expandtab shiftwidth=4
