-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Combine this release with the matrix history.

insert into matrix_history_updated
    (collection, base_id, version, name, release)
    select collection, matrix_updated.base_id, matrix_updated.version, name, '2022'
    from matrix_updated
    inner join (
        select base_id, max(version) as version
        from matrix_updated
        group by base_id
        ) as latest
        on matrix_updated.base_id = latest.base_id
        and matrix_updated.version = latest.version;

-- vim: tabstop=4 expandtab shiftwidth=4
