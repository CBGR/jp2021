-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Obtain unchanged profiles: those either not appearing in the incoming records
-- or old versions of updated records.
-- Here, the identifier mapping contains existing records that are updated, and
-- so any other existing records (absent from the mapping) are therefore
-- unchanged.

insert into matrix_updated
    select matrix.id,
           matrix.collection,
           matrix.base_id,
           matrix.version,
           matrix.name
    from matrix
    left outer join existing_identifier_mapping
        on matrix.id = existing_identifier_mapping.id
    where existing_identifier_mapping.id is null;

analyze matrix_updated;

-- Obtain matrix data for unchanged profiles.

insert into matrix_data_updated
    select matrix_data.id,
           matrix_data.row,
           matrix_data.col,
           matrix_data.val
    from matrix_data
    inner join matrix_updated
        on matrix_data.id = matrix_updated.id;

-- Obtain species data for unchanged profiles.

insert into matrix_species_updated
    select matrix_species.id,
           matrix_species.tax_id
    from matrix_species
    inner join matrix_updated
        on matrix_species.id = matrix_updated.id;

-- Obtain annotation data for unchanged profiles.

insert into matrix_annotation_updated
    select matrix_annotation.id,
           matrix_annotation.tag,
           matrix_annotation.val
    from matrix_annotation
    inner join matrix_updated
        on matrix_annotation.id = matrix_updated.id;

-- Obtain protein data for unchanged profiles.

insert into matrix_protein_updated
    select matrix_protein.id,
           matrix_protein.acc
    from matrix_protein
    inner join matrix_updated
        on matrix_protein.id = matrix_updated.id;

-- Aggregate the existing file details for unchanged profiles.

insert into matrix_file_details_updated
    (filetype, base_id, version, filename)
    select filetype, F.base_id, F.version, filename
    from matrix_file_details as F
    inner join matrix as M
        on F.base_id = M.base_id
        and F.version = M.version
    left outer join existing_identifier_mapping
        on M.id = existing_identifier_mapping.id
    where existing_identifier_mapping.id is null;

-- vim: tabstop=4 expandtab shiftwidth=4
