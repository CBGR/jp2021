-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Update annotations for otherwise unchanged profiles.



-- Obtain the affected profile versions.

create temporary table tmp_affected as
    select matrix.id as id,
           matrix_incoming.id as arbitrary_id,
           matrix_incoming.name as name,
           action
    from matrix_incoming
    inner join matrix
        on matrix_incoming.base_id = matrix.base_id
        and matrix_incoming.version = matrix.version
    inner join matrix_action_incoming
        on matrix_incoming.id = matrix_action_incoming.id
    where action = 'P';

-- Remove all annotations for the affected versions.

delete from matrix_annotation_updated
where id in (select id from tmp_affected);

-- Insert annotations for the affected versions.

insert into matrix_annotation_updated
    select tmp_affected.id,
           A.tag,
           A.val
    from tmp_affected
    inner join matrix_annotation_incoming as A
        on tmp_affected.arbitrary_id = A.id;

-- Replace naming information.

update matrix_updated
    set name = (
        select name
        from tmp_affected
        where tmp_affected.id = matrix_updated.id)
    where id in (
        select id
        from tmp_affected
        where name is not null);

-- vim: tabstop=4 expandtab shiftwidth=4
