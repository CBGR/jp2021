-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Fixed file details.

create table fixed_bed_fasta_files (
    base_id varchar(16) not null,
    version integer not null,
    bed_file varchar(255) not null,
    fasta_file varchar(255) not null,
    primary key (base_id, version)
);

-- vim: tabstop=4 expandtab shiftwidth=4
