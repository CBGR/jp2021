-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Introduce replacement profiles for 'A' and 'V' actions where the curation
-- data references existing profiles. The existing profiles will be excluded.
-- However, the primary key will be retained from each replaced profile in the
-- replacement record.

insert into matrix_updated
    select existing_identifier_mapping.id,
           'CORE',
           'MA????' as base_id,
           1 as version,
           matrix_incoming.name
    from matrix_incoming
    inner join existing_identifier_mapping
        on matrix_incoming.id = existing_identifier_mapping.arbitrary_id
    where action in ('A', 'V');

-- Obtain matrix data for assigned profiles from the incoming data files.

insert into matrix_data_updated
    select existing_identifier_mapping.id,
           matrix_data_incoming.row,
           matrix_data_incoming.col,
           matrix_data_incoming.val
    from matrix_data_incoming
    inner join existing_identifier_mapping
        on matrix_data_incoming.id = existing_identifier_mapping.arbitrary_id
    where action = 'A';

-- Validated, existing profiles do not use the incoming PFM data, but instead
-- preserve the existing PFM data.

insert into matrix_data_updated
    select matrix_data.id,
           matrix_data.row,
           matrix_data.col,
           matrix_data.val
    from matrix_data
    inner join existing_identifier_mapping
        on matrix_data.id = existing_identifier_mapping.id
    where action = 'V';

-- Obtain species data for updated profiles from the incoming data files.

insert into matrix_species_updated
    select existing_identifier_mapping.id,
           matrix_species_incoming.tax_id
    from matrix_species_incoming
    inner join existing_identifier_mapping
        on matrix_species_incoming.id = existing_identifier_mapping.arbitrary_id
    where action in ('A', 'V');

-- Obtain annotation data for updated profiles excluding comments since these
-- are curation-related comments in the incoming curation data.

insert into matrix_annotation_updated
    select existing_identifier_mapping.id,
           matrix_annotation_incoming.tag,
           matrix_annotation_incoming.val
    from matrix_annotation_incoming
    inner join existing_identifier_mapping
        on matrix_annotation_incoming.id = existing_identifier_mapping.arbitrary_id
    where action in ('A', 'V');

-- Obtain protein data for updated profiles from the incoming data files.

insert into matrix_protein_updated
    select existing_identifier_mapping.id,
           matrix_protein_incoming.acc
    from matrix_protein_incoming
    inner join existing_identifier_mapping
        on matrix_protein_incoming.id = existing_identifier_mapping.arbitrary_id
    where action in ('A', 'V');

-- Aggregate the incoming file details for updated profiles.

insert into matrix_file_details_updated
    (filetype, base_id, version, filename)
    select 'bed', base_id, version, bed_file
    from matrix_files_incoming as F
    inner join existing_identifier_mapping as N
        on F.id = N.arbitrary_id
    inner join matrix_incoming as M
        on F.id = M.id
    where action in ('A', 'V')
        and bed_file is not null
    union all
    select 'fasta', base_id, version, fasta_file
    from matrix_files_incoming as F
    inner join existing_identifier_mapping as N
        on F.id = N.arbitrary_id
    inner join matrix_incoming as M
        on F.id = M.id
    where action in ('A', 'V')
        and fasta_file is not null
    union all
    select 'centrality', base_id, version, centrality_file
    from matrix_files_incoming as F
    inner join existing_identifier_mapping as N
        on F.id = N.arbitrary_id
    inner join matrix_incoming as M
        on F.id = M.id
    where action in ('A', 'V')
        and centrality_file is not null;



-- Archive replaced profile details.

insert into matrix_archived
    select existing_identifier_mapping.id,
           matrix_incoming.collection,
           matrix_incoming.base_id,
           matrix_incoming.version,
           matrix_incoming.name
    from matrix_incoming
    inner join existing_identifier_mapping
        on matrix_incoming.id = existing_identifier_mapping.arbitrary_id
    where action in ('A', 'V');

insert into matrix_data_archived
    select existing_identifier_mapping.id,
           matrix_data.row,
           matrix_data.col,
           matrix_data.val
    from matrix_data
    inner join existing_identifier_mapping
        on matrix_data.id = existing_identifier_mapping.id
    where action in ('A', 'V');

insert into matrix_species_archived
    select existing_identifier_mapping.id,
           matrix_species.tax_id
    from matrix_species
    inner join existing_identifier_mapping
        on matrix_species.id = existing_identifier_mapping.id
    where action in ('A', 'V');

insert into matrix_annotation_archived
    select existing_identifier_mapping.id,
           matrix_annotation.tag,
           matrix_annotation.val
    from matrix_annotation
    inner join existing_identifier_mapping
        on matrix_annotation.id = existing_identifier_mapping.id
    where action in ('A', 'V');

insert into matrix_protein_archived
    select existing_identifier_mapping.id,
           matrix_protein.acc
    from matrix_protein
    inner join existing_identifier_mapping
        on matrix_protein.id = existing_identifier_mapping.id
    where action in ('A', 'V');

-- vim: tabstop=4 expandtab shiftwidth=4
