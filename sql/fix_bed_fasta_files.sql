-- SPDX-FileCopyrightText: 2021 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Revise matrix filename details.

delete from matrix_file_details_updated
where filetype in ('bed', 'fasta')
    and exists (
        select base_id, version from fixed_bed_fasta_files as F
        where F.base_id = matrix_file_details_updated.base_id
            and F.version = matrix_file_details_updated.version);

insert into matrix_file_details_updated
    (filetype, base_id, version, filename)
    select 'bed', base_id, version, bed_file
    from fixed_bed_fasta_files
    union all
    select 'fasta', base_id, version, fasta_file
    from fixed_bed_fasta_files;

-- vim: tabstop=4 expandtab shiftwidth=4
