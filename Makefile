# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Processing workflow consuming existing data and curation data, producing
# updated database records and data archives.



# Special definitions for release archives, directories, existing data, exported
# data, grouped targets (where multiple files depend on other files), installed
# files, PFM data, previously archived PFM data, referenced/cached data files
# and TFFM data.

# These definitions typically involve release-specific processing, meaning that
# they are applied to more than one different database release.

# More includes occur at the end of this file to make use of definitions
# featured in this file.

include $(CURDIR)/mk/archives.mk
include $(CURDIR)/mk/archives_clustering.mk
include $(CURDIR)/mk/archives_database.mk
include $(CURDIR)/mk/archives_tffm.mk
include $(CURDIR)/mk/clustering.mk
include $(CURDIR)/mk/directories.mk
include $(CURDIR)/mk/existing_data.mk
include $(CURDIR)/mk/export_data.mk
include $(CURDIR)/mk/grouped.mk
include $(CURDIR)/mk/install.mk
include $(CURDIR)/mk/matrix_history.mk
include $(CURDIR)/mk/pfm.mk
include $(CURDIR)/mk/pfm_archived.mk
include $(CURDIR)/mk/referenced_data.mk
include $(CURDIR)/mk/revise_existing.mk
include $(CURDIR)/mk/synonyms.mk
include $(CURDIR)/mk/tffm_data.mk



# Program locations and definitions.

BASEDIR = $(CURDIR)
DATA = $(BASEDIR)/data
TOOLS = $(BASEDIR)/tools
DATATOOLS = $(TOOLS)/data
SQLTOOLS = $(TOOLS)/sqlite
LINK = ln -s
PROCESSES = 8



# Data locations and sources.

WORK				?= $(BASEDIR)/work
OUTPUT_DATA			?= /var/tmp/jaspar-data

# Curation input data.
# These details are sensitive to the way the curation data is prepared, needing
# attention for subsequent releases.

CURATION_DATA			= JASPAR2022_curation.ods
CURATION_TABLE			= JASPAR_2022_curated_motifs
DELETION_TABLE			= "Profiles to delete"
CHANGELOG_DATA			= Changelog_JASPAR_2022.odt

# Curation revision data.
# This is applied to the database after previous processing of the curation data
# to revise it and produce new output data.

CURATION_REVISION_DATA		= JASPAR2022_curation_revision.ods
CURATION_REVISION_TABLE		= JASPAR_2022_REVISION_edits
DELETION_REVISION_TABLE		= JASPAR_2022_REVISION_profiles_to_delete
CHANGELOG_REVISION_DATA		= Changelog_JASPAR_2022_revision.odt

# Existing data location.
# The description is used for error messages.

JASPAR_DATA_DIR			?= ../JASPAR/data
JASPAR_DATA_DIR_DESCRIPTION	= a directory containing JASPAR database files

# Existing releases and the new release being constructed, related to the
# resources below.

# Previous releases before the latest (current) release.

JASPAR_RELEASES_WITHOUT_TFFM	= 2014
JASPAR_RELEASES_WITH_TFFM	= 2016 2018
JASPAR_RELEASES_PREVIOUS	= $(JASPAR_RELEASES_WITHOUT_TFFM) $(JASPAR_RELEASES_WITH_TFFM)

# The latest existing release is treated specially, being the basis of the new
# release in certain respects. For subsequent new releases, the latest release
# is added to the previous lists, the former new release moved to the latest
# release, and the actual new release introduced.

JASPAR_RELEASE_LATEST		= 2020
JASPAR_RELEASE_NEW		= 2022

# Special identifiers are used for revised and fixed releases.

JASPAR_RELEASE_NEW_REVISED	= $(JASPAR_RELEASE_NEW)_revised
JASPAR_RELEASE_NEW_FIXED	= $(JASPAR_RELEASE_NEW)_revised_fixed

# Releases involving archetypes and clustering data.

JASPAR_ALL_ARCHETYPES_RELEASES	= 2022
JASPAR_ALL_CLUSTERING_RELEASES	= 2020 2022



# Processing inputs and products.

include $(CURDIR)/mk/inputs.mk
include $(CURDIR)/mk/cached.mk
include $(CURDIR)/mk/products.mk



# Processing workflow.

.PHONY : all almost-clean check check_fixed check_revised clean fixed help \
	 install install_fixed install_revised install_tffm_revised revised tffm tffm_revised

all: $(OUTPUTS_NON_TFFM)

revised: $(OUTPUTS_NON_TFFM_REVISED)

fixed: $(OUTPUTS_NON_TFFM_FIXED)

tffm: $(OUTPUTS_TFFM)

tffm_revised: $(OUTPUTS_TFFM_REVISED)

check: $(INCOMING_METADATA)/matrix_files
	@$(DATATOOLS)/check_incoming_matrix_files $(INCOMING_METADATA)/matrix_files 2

check_revised: $(INCOMING_METADATA_REVISED)/matrix_files
	@$(DATATOOLS)/check_incoming_matrix_files $(INCOMING_METADATA_REVISED)/matrix_files 2

check_fixed: $(INCOMING_METADATA_FIXED)/matrix_files
	@$(DATATOOLS)/check_incoming_matrix_files $(INCOMING_METADATA_FIXED)/matrix_files 3

check_tffm: $(UPDATED_TFFM_FILES)
	@$(DATATOOLS)/check_tffm_files $(UPDATED_TFFM_FILES) $(INCOMING_TFFM_DIR)

check_tffm_revised: $(UPDATED_TFFM_FILES_REVISED)
	@$(DATATOOLS)/check_tffm_files $(UPDATED_TFFM_FILES_REVISED) $(INCOMING_TFFM_DIR)

almost-clean:
	$(RM) $(ALMOST_ALL_OUTPUT_FILES)
	$(RM) -r $(ALMOST_ALL_OUTPUT_DIRECTORIES)

clean:
	$(RM) $(ALL_OUTPUT_FILES)
	$(RM) -r $(ALL_OUTPUT_DIRECTORIES)
	-rmdir $(WORK)

install: $(OUTPUTS_INSTALLED_NON_TFFM) $(OUTPUTS_INSTALLED_NO_BACKUP)

install_revised: $(OUTPUTS_INSTALLED_NON_TFFM_REVISED) $(OUTPUTS_INSTALLED_NO_BACKUP_REVISED)

install_fixed: $(OUTPUTS_INSTALLED_NON_TFFM_FIXED) $(OUTPUTS_INSTALLED_NO_BACKUP_REVISED)

install_tffm: $(OUTPUTS_INSTALLED_TFFM)

install_tffm_revised: $(OUTPUTS_INSTALLED_TFFM_REVISED)

help:
	@echo "== Usage =="
	@echo
	@echo "make [ <target> ] [ <definition> ... ]"
	@echo
	@echo "== Targets =="
	@echo
	@echo "help                 - show this message"
	@echo
	@echo "clean                - remove all working files"
	@echo "almost-clean         - remove all working files except cached data"
	@echo
	@echo "check                - check for referenced files"
	@echo "check_revised        - check for revised referenced files"
	@echo "check_tffm           - check for TFFM files"
	@echo
	@echo "all                  - build matrix profile data and metadata, excluding TFFMs"
	@echo "revised              - build revised matrix profile data and metadata"
	@echo "tffm                 - build TFFM data and metadata"
	@echo "tffm_revised         - build revised TFFM data and metadata"
	@echo "fixed                - build fixed, revised matrix profile data"
	@echo
	@echo "install              - install matrix profile data and metadata"
	@echo "install_revised      - install revised matrix profile data and metadata"
	@echo "install_tffm         - install TFFM data and metadata"
	@echo "install_tffm_revised - install revised TFFM data and metadata"
	@echo "install_fixed        - install fixed, revised matrix profile data"
	@echo
	@echo "== Definitions =="
	@echo
	@echo "Output/installation directory:"
	@echo
	@echo "OUTPUT_DATA=$(OUTPUT_DATA)"
	@echo
	@echo "Input data details:"
	@echo
	@echo "JASPAR_DATA_DIR=$(JASPAR_DATA_DIR)"
	@echo
	@echo "Curation table:"
	@echo
	@echo "CURATION_DATA=$(CURATION_DATA)"
	@echo "CURATION_TABLE=$(CURATION_TABLE)"
	@echo "DELETION_TABLE=$(DELETION_TABLE)"
	@echo
	@echo "Revised curation table:"
	@echo
	@echo "CURATION_REVISION_DATA=$(CURATION_REVISION_DATA)"
	@echo "CURATION_REVISION_TABLE=$(CURATION_REVISION_TABLE)"
	@echo "DELETION_REVISION_TABLE=$(DELETION_REVISION_TABLE)"
	@echo
	@echo "Changelog details:"
	@echo
	@echo "CHANGELOG_DATA=$(CHANGELOG_DATA)"
	@echo "CHANGELOG_REVISION_DATA=$(CHANGELOG_REVISION_DATA)"
	@echo
	@echo "Internal work directory:"
	@echo
	@echo "WORK=$(WORK)"

# Define an install rule for each output.

$(foreach OUTPUT,$(OUTPUTS_NON_TFFM),\
$(eval $(call install_template,$(OUTPUT))))

$(foreach OUTPUT,$(OUTPUTS_TFFM),\
$(eval $(call install_template,$(OUTPUT))))

$(foreach OUTPUT,$(OUTPUTS_NO_BACKUP),\
$(eval $(call install_no_backup_template,$(OUTPUT))))

# Install rules are also generated for revised data.

$(foreach OUTPUT,$(OUTPUTS_NON_TFFM_REVISED),\
$(eval $(call install_template,$(OUTPUT))))

$(foreach OUTPUT,$(OUTPUTS_NON_TFFM_FIXED),\
$(eval $(call install_template,$(OUTPUT))))

$(foreach OUTPUT,$(OUTPUTS_TFFM_REVISED),\
$(eval $(call install_template,$(OUTPUT))))

$(foreach OUTPUT,$(OUTPUTS_NO_BACKUP_REVISED),\
$(eval $(call install_no_backup_template,$(OUTPUT))))



# Make sure that the work and output directories exist.

$(ALL_OUTPUT_FILES): | $(WORK)

$(OUTPUTS_INSTALLED_ALL): | $(OUTPUT_DATA)

# Create various work and output directories if needed.

$(foreach DIRECTORY,$(ALL_DIRECTORIES),\
$(eval $(call directory_template,$(DIRECTORY))))



# Archive production.

$(ARCHIVE_TFFM): $(TFFM_COMPLETE_DATA_MARKER)
	$(TOOLS)/tar_create $(ARCHIVE_TFFM) $(TFFM_COMPLETE_DATA)

$(ARCHIVE_WORDCLOUDS): $(WORDCLOUDS_MARKER)
	$(TOOLS)/tar_create $(ARCHIVE_WORDCLOUDS) $(WORDCLOUDS)

# General data, metadata and download data archives for each release.

$(foreach RELEASE,$(JASPAR_ALL_RELEASES),\
$(eval $(call archives_template,$(RELEASE))))

$(foreach RELEASE,$(JASPAR_ALL_CLUSTERING_RELEASES),\
$(eval $(call archives_clustering_template,$(RELEASE))))

$(eval $(call archives_database_template,$(JASPAR_RELEASE_NEW)))

$(foreach RELEASE,$(JASPAR_ALL_TFFM_RELEASES),\
$(eval $(call archives_tffm_template,$(RELEASE))))

# Archiving rules are also generated for any revised releases.

$(ARCHIVE_TFFM_REVISED): $(TFFM_COMPLETE_DATA_MARKER_REVISED)
	$(TOOLS)/tar_create $(ARCHIVE_TFFM_REVISED) $(TFFM_COMPLETE_DATA_REVISED)

$(foreach RELEASE,$(JASPAR_ALL_RELEASES_REVISED),\
$(eval $(call archives_template,$(RELEASE))))

$(eval $(call archives_database_template,$(JASPAR_RELEASE_NEW_REVISED)))

$(foreach RELEASE,$(JASPAR_RELEASE_NEW_REVISED),\
$(eval $(call archives_tffm_template,$(RELEASE))))

# Matrix history export.

$(UPDATED_MATRIX_HISTORY): $(UPDATED_METADATA)/matrix_history
	cut -f 2- $< > $@

$(UPDATED_MATRIX_HISTORY_REVISED): $(UPDATED_METADATA_REVISED)/matrix_history
	cut -f 2- $< > $@



# Reference indicated or previously used data via symbolic links.

$(foreach FILENAME,$(JASPAR_ALL_DATA),\
$(eval $(call referenced_data_template,JASPAR_DATA_DIR,$(FILENAME),$(JASPAR_DATA_DIR_DESCRIPTION))))

# Existing releases are prepared from database archives.

$(foreach RELEASE,$(JASPAR_EXISTING_RELEASES),\
$(eval $(call existing_data_template,$(RELEASE))))

$(foreach RELEASE,$(JASPAR_ALL_TFFM_RELEASES),\
$(eval $(call tffm_data_template,$(RELEASE))))

# TFFM rules are also generated for any revised release.
# Here, the revised release is treated as a variant of the new release.

$(eval $(call tffm_data_template,$(JASPAR_RELEASE_NEW),_revised))

# Data is exported from all releases for further processing.

$(foreach RELEASE,$(JASPAR_ALL_RELEASES),\
$(eval $(call export_data_template,$(RELEASE))))

# Export rules are also generated for any revised release.
# Here, the revised release is treated as a variant of the new release.

$(eval $(call export_data_template,$(JASPAR_RELEASE_NEW),_revised))

# Matrix history is propagated to earlier releases.

$(foreach RELEASE,$(JASPAR_EXISTING_RELEASES),\
$(eval $(call matrix_history_template,$(RELEASE),$(JASPAR_RELEASE_NEW))))

# Revised metadata is also propagated to earlier revised releases.

$(foreach RELEASE,$(JASPAR_EXISTING_RELEASES_REVISED),\
$(eval $(call matrix_history_template,$(RELEASE),$(JASPAR_RELEASE_NEW_REVISED))))

# Revised metadata for earlier releases is based on those releases.

$(foreach RELEASE,$(JASPAR_EXISTING_RELEASES),\
$(eval $(call revise_existing_template,$(RELEASE))))

# Synonym data is prepared for all releases.

$(foreach RELEASE,$(JASPAR_ALL_RELEASES),\
$(eval $(call synonyms_template,$(RELEASE))))

# Synonym rules are also generated for any revised release.

$(eval $(call synonyms_template,$(JASPAR_RELEASE_NEW_REVISED)))

# Produce PFM data for all releases including archived PFM data.

$(foreach RELEASE,$(JASPAR_ALL_PFM_RELEASES),\
$(eval $(call pfm_template,$(RELEASE))))

# PFM rules are also generated for any revised release including archived PFM
# data.

$(foreach RELEASE,$(JASPAR_PFM_RELEASES_REVISED),\
$(eval $(call pfm_template,$(RELEASE))))

# Archived PFM data for removed profiles is combined with the generated data to
# create a complete record of the logos.

$(eval $(call pfm_archived_template,$(JASPAR_RELEASE_NEW)))

# Archived PFM rules are also generated for any revised release.

$(eval $(call pfm_archived_template,$(JASPAR_RELEASE_NEW_REVISED)))

# Unpack and process clustering data.

$(foreach RELEASE,$(JASPAR_ALL_CLUSTERING_RELEASES),\
$(eval $(call clustering_template,$(RELEASE))))



# Includes depending on definitions above for conversion of input documents,
# processing of curation data, preparation of curation data from incoming data,
# processing of matrix data files, naming data, taxonomy data, TFFM data,
# wordcloud image data, and work directory creation.

include $(CURDIR)/mk/conversion.mk
include $(CURDIR)/mk/curation.mk
include $(CURDIR)/mk/incoming.mk
include $(CURDIR)/mk/matrix_files.mk
include $(CURDIR)/mk/naming.mk
include $(CURDIR)/mk/tax.mk
include $(CURDIR)/mk/tffm.mk
include $(CURDIR)/mk/wordclouds.mk
include $(CURDIR)/mk/work.mk

# The following is to handle fixed data archives.

include $(CURDIR)/mk/archives_fixed.mk
