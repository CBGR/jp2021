<?xml version="1.0"?>

<!--
SPDX-FileCopyrightText: 2021 University of Oslo
SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
SPDX-License-Identifier: GPL-3.0-or-later
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
                              xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
                              exclude-result-prefixes="office text">

  <xsl:param name="changelog-version" />
  <xsl:output method="html" />

  <!-- Translate only the text section of the document. -->

  <xsl:template match="/">
    <xsl:apply-templates select="//office:text" />
  </xsl:template>

  <!-- Create a top-level element to hold the changelog entry. -->

  <xsl:template match="office:text">
    <div class="{$changelog-version}">
      <xsl:if test="$changelog-version">
        <h3><xsl:value-of select="$changelog-version" /></h3>
      </xsl:if>
      <xsl:apply-templates select="text:list[local-name(preceding-sibling::*[1]) != 'list']|text:p|text:span" />
    </div>
  </xsl:template>

  <!-- Create lists using the first list element found in any element sequence. -->

  <xsl:template match="text:list[local-name(preceding-sibling::*[1]) != 'list']">
    <ul>
      <xsl:apply-templates select="text:list-item[.//text:span|.//text:p]" />
      <xsl:if test="local-name(following-sibling::*[1]) = 'list'">
        <xsl:apply-templates select="following-sibling::*[1]" />
      </xsl:if>
    </ul>
  </xsl:template>

  <!-- Merge adjacent lists into preceding ones. -->

  <xsl:template match="text:list[local-name(preceding-sibling::*[1]) = 'list']">
    <xsl:apply-templates select="text:list-item[.//text:span|.//text:p]" />
  </xsl:template>

  <!-- Items are translated directly. -->

  <xsl:template match="text:list-item">
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>

  <!-- Paragraph elements provide actual text. -->

  <xsl:template match="text:p">
    <xsl:if test="text()|text:span">
      <p><xsl:value-of select="text()|text:span" /></p>
    </xsl:if>
  </xsl:template>

  <!-- Span elements provide actual text. -->

  <xsl:template match="text:span">
    <span><xsl:value-of select="text()" /></span>
  </xsl:template>

</xsl:stylesheet>

<!-- vim: tabstop=2 expandtab shiftwidth=2
     -->
