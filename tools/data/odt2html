#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Convert a changelog from ODT format to HTML for inclusion in the Web
# application.

THISDIR=`dirname "$0"`
PROGNAME=`basename "$0"`

# Main program.

FILENAME=$1
VERSION=$2
OUTPUT_FILE=$3

if [ ! -e "$FILENAME" ] || [ ! "$VERSION" ] || [ ! "$OUTPUT_FILE" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <changelog file> <version label> <output file>

Convert the given changelog file to HTML, annotating it with the given version
label, and writing the converted document to the indicated output file
EOF
    exit 1
fi

# Create the output directory if necessary.

OUTPUT_DIR=`dirname "$OUTPUT_FILE"`

if [ ! -e "$OUTPUT_DIR" ] ; then
    mkdir -p "$OUTPUT_DIR"
fi

# Extract the XML content from the document archive.

TEMPDIR=`mktemp -d`
unzip -d "$TEMPDIR" "$FILENAME" content.xml

# Convert using the stylesheet, indicating a version for annotation.

  xsltproc --param changelog-version "$VERSION" "$THISDIR/odt2html.xsl" "$TEMPDIR/content.xml" \
> "$OUTPUT_FILE"

# Remove the extracted content.

rm -rf "$TEMPDIR"

# vim: tabstop=4 expandtab shiftwidth=4
