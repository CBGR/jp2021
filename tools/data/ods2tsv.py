#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Convert JASPAR curation data in ODF to tab-separated format. This tool treats
cell data in a simplistic fashion, merely concatenating the textual form of each
child node of a cell element, this causing slight differences in whitespace
handling in the curation data, although actually eliminating apparently
superfluous spaces introduced by certain spurious formatting elements.
"""

from odf.opendocument import load, FONS, TABLENS, TEXTNS
from odf.style import Style, TableCellProperties
from odf.table import TableCell, TableRow
from jp2021 import write
from os.path import split
import sys

def get_cell_colours(styles):

    """
    Find cell style definitions in 'styles' and construct a mapping from style
    names to cell background colours.
    """

    colours = {}

    for style_element in styles.getElementsByType(Style):
        for properties_element in style_element.getElementsByType(TableCellProperties):
            colours[style_element.getAttribute("name")] = properties_element.getAttrNS(FONS, "background-color")

    return colours

def convert(table, colours):

    """
    Convert 'table' to tab-separated format, emitting the result on standard
    output.
    """

    column_limit = None

    for row_element in table.getElementsByType(TableRow):
        row = []

        # Obtain any colour information.

        if colours:
            colour_values = set()

            for cell_element in row_element.getElementsByType(TableCell):
                style_name = cell_element.getAttrNS(TABLENS, "style-name")
                colour_value = colours.get(style_name)

                if colour_value:
                    colour_values.add(colour_value)

            row.append("|".join(colour_values))

        for cell_element in row_element.getElementsByType(TableCell):
            cell = []

            # Collect textual cell value data only.

            for value_element in cell_element.childNodes:
                ns, local_name = value_element.qname
                if ns == TEXTNS:
                    cell.append(str(value_element))

            # Add the cell multiple times if indicated. It doesn't look like
            # this is actually repeating as opposed to indicating a number
            # of occurrences of a cell.

            repeat = int(cell_element.getAttrNS(TABLENS, "number-columns-repeated") or "1")

            while repeat:
                row.append("".join(cell))
                repeat -= 1

        # Remove trailing empty cells beyond the initial row's columns.

        i = len(row)

        while i >= (column_limit or 0):
            i -= 1
            if row[i]:
                break

        row = row[:i+1]

        if column_limit is None:
            column_limit = i + 1

        # Omit any empty rows.

        if list(filter(None, row)):
            write(separator.join(row))

def extract(items):

    "Extract the next item from 'items', returning None if no more remain."

    if items:
        item = items[0]
        del items[0]
        return item
    else:
        return None

def extract_option(items, option):

    """
    Extract from 'items' the given 'option', removing it if present. Return
    True if the option was present, False otherwise.
    """

    if option in items:
        del items[items.index(option)]
        return True
    else:
        return False

# Main program.

if __name__ == "__main__":
    progname = split(sys.argv[0])[-1]

    try:
        filename = sys.argv[1]
        table_name = sys.argv[2]
    except IndexError:
        print("Usage: %s <filename> ( -l | <table name> [ -c ] [ <separator> ] )" % progname,
              file=sys.stderr)
        sys.exit(1)

    list_tables = table_name == "-l"
    remaining = sys.argv[3:]
    show_cell_colours = extract_option(remaining, "-c")
    separator = extract(remaining) or "\t"

    doc = load(filename)

    # Look for tables (tabs) in the spreadsheet.

    for element in doc.spreadsheet.childNodes:
        if element.qname[1] == "table":
            name = element.getAttribute("name")

            # Either show table names or select any indicated table.

            if list_tables:
                write(name)
            elif name == table_name:
                break
    else:
        # At the end of listing tables or having not found any indicated table,
        # exit appropriately.

        if list_tables:
            sys.exit(0)
        else:
            print("Table %s not found in file %s." % (table_name, filename), file=sys.stderr)
            sys.exit(1)

    # Having found the indicated table, convert the representation.
    # The cell colours are passed to the conversion for filtering, if indicated.

    if show_cell_colours:
        colours = get_cell_colours(doc.automaticstyles)
    else:
        colours = None

    convert(element, colours)

# vim: tabstop=4 expandtab shiftwidth=4
