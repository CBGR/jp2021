#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
TOOLS=`realpath "$THISDIR/.."`
BASEDIR=`realpath "$THISDIR/../.."`
PROGNAME=`basename "$0"`

if [ "$1" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME

Obtain column data from a tab-separated curation table provided on standard
input, numbering each record, writing the table to standard output. The output
should provide matrix protein records.

Where multiple proteins are specified for each incoming record, these proteins
create multiple output records with the same key or record identifier.
EOF
    exit 0
fi

# Obtain the column names.

COLUMNS=`grep '^proteins' "$BASEDIR/data/incoming" | cut -f 2`

# Read, select, number, separate, strip and write the records.

  "$TOOLS/cutbyname" -f "$COLUMNS" \
| "$TOOLS/number_lines" \
| "$TOOLS/combinations" - '::' \
| "$TOOLS/combinations" - '; ' \
| sed 's/  *$//'

# vim: tabstop=4 expandtab shiftwidth=4
