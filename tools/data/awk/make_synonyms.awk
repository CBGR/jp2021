# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Tab-separate the input and output.

BEGIN {
  FS = "\t"
  OFS = "\t"
}

# Process gene_info files.
#
# Relevant columns:
#
# 1: taxid
# 2: geneid
# 3: symbol
# 4: locus tag
# 5: synonyms
# 11: symbol (authoritative)
# 12: name (authoritative)
# 14: other names

{
  # Ignore NEWENTRY records.

  if ($3 != "NEWENTRY")
  {
    # Quote backslash characters.

    gsub(/\\/, "\\\\", $3);
    gsub(/\\/, "\\\\", $5);
    gsub(/\\/, "\\\\", $11);
    gsub(/\\/, "\\\\", $12);
    gsub(/\\/, "\\\\", $14);

    # Employ symbols as synonyms if no synonyms of any kind are defined.

    if (($4 == "-") && ($5 == "-") && ($11 == "-") && ($12 == "-") && ($14 == "-"))
      print $1, $2, $3, $3

    # Output any differing locus tag.

    if (($4 != "-") && ($4 != $3))
      print $1, $2, $3, $4

    # Output any differing authoritative symbol.

    if (($11 != "-") && ($11 != $3))
      print $1, $2, $3, $11

    # Output any differing authoritative name.

    if (($12 != "-") && ($12 != $3))
      print $1, $2, $3, $12

    # Expand any synonyms.

    if ($5 != "-")
      expand($5)

    # Expand any other names.

    if ($14 != "-")
      expand($14)
  }
}

function expand(value)
{
  # Obtain individual synonyms and replicate the record for each one.

  split(value, names, "|")
  for (i in names)
    print $1, $2, $3, names[i]
}
