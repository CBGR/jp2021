# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Tab-separate the output.

BEGIN {
  FS = "   "
  OFS = "\t"
}

# Start of entry.

/^ID/ {
  identifier = $2
  accessions = ""
}

/^AC/ {
  accessions = accessions $2
}

# End of entry.

/^\/\// {

  # The semicolon terminates each item.

  split(accessions, items, "; *")

  for (i in items)
  {
    # The last item will be blank.

    # It appears that the items may not be iterated over in order, this being
    # seen with awk 4.0.2, so detecting the final item should not terminate the
    # loop. In fact, this appears to be a documented characteristic of the tool:
    # http://www.gnu.org/software/gawk/manual/html_node/Scanning-an-Array.html

    if (i == length(items))
      continue

    print identifier, items[i]
  }
}
