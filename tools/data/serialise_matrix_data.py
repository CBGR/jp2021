#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from Bio import motifs
from jp2021 import write
from os.path import splitext
from io import StringIO
import sys

rows = ["A", "C", "G", "T"]

def write_matrix_data(matrix_id, filename, write=write):

    "Write data for 'matrix_id' from 'filename' using 'write'."

    basename, ext = splitext(filename)
    ext = ext.lstrip(".")

    # Handle non-standard naming usage.

    if ext == "trimmed":
        ext = "jaspar"
    elif ext == "tf":
        ext = "transfac"

    # Parse the file.

    with open(filename) as f:

        # Fix up non-conformant data.

        if ext == "transfac":
            f = fix_transfac(f)

        m = motifs.read(f, ext)

    # Output the counts data in tabular form.

    for row in rows:
        for col, val in enumerate(m.counts[row], 1):
            record = map(str, [matrix_id, row, col, val])
            write("\t".join(record))

def fix_transfac(f):

    "Fix up non-conformant data in TRANSFAC input."

    out = StringIO()
    for line in f.readlines():

        # Upper-case matrix headers.

        if line.startswith("P0"):
            line = line.upper()

        # Fix up matrix numbering.

        else:
            columns = line.split()
            if columns[0].isdigit() and len(columns[0]) < 2:
                line = "  ".join(["%02d" % int(columns[0])] + columns[1:]) + "\n"

        out.write(line)

    out.seek(0)
    return out

# Main program.

if __name__ == "__main__":
    f = sys.stdin

    # Loop over incoming lines, writing matrix data.

    matrix_id = 1
    line = f.readline()

    while line:
        line = line.strip()
        if line:
            write_matrix_data(matrix_id, line)
        line = f.readline()
        matrix_id += 1

# vim: tabstop=4 expandtab shiftwidth=4
