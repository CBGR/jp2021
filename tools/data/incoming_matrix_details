#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
PROGNAME=`basename "$0"`
BASEDIR=`realpath "$THISDIR/../.."`
DATA="$BASEDIR/data"

INPUT_DATA=$1
OUTPUT_DIR=$2

if [ ! -e "$INPUT_DATA" ] || [ ! "$OUTPUT_DIR" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <input data> <output directory>

Process incoming input data, being a curation table in tab-separated form,
writing different types of data to the indicated output directory.
EOF
    exit 0
fi

# Make the output directory if absent.

if [ ! -e "$OUTPUT_DIR" ] ; then
    mkdir -p "$OUTPUT_DIR"
fi

# Invoke separate scripts to produce the output files.

for DATA_TYPE in actions data files profiles proteins species ; do
    if ! "$THISDIR/incoming_matrix_$DATA_TYPE" < "$INPUT_DATA" > "$OUTPUT_DIR/matrix_$DATA_TYPE" ; then
        echo "Processing of $DATA_TYPE failed for incoming data." 1>&2
        exit 1
    fi
done

# Annotations are obtained from a number of different columns. These are defined
# in a separate file. Here, each of the column names is read from the file and
# supplied to a command invocation, with the aggregated output being written to
# a single file.

  cut -f 2 "$DATA/tag_synonyms" \
| xargs -I{} sh -c "\"$THISDIR/incoming_matrix_annotations\" '{}' < \"$INPUT_DATA\"" \
> "$OUTPUT_DIR/matrix_annotations"

# vim: tabstop=4 expandtab shiftwidth=4
