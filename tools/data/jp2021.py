#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Common functions for input/output handling.
"""

import os, sys

def handle_broken_pipe():

    """
    Horrid workaround for Python 3 input/output handling.

    See: https://docs.python.org/3/library/signal.html#note-on-sigpipe
    """

    devnull = os.open(os.devnull, os.O_WRONLY)
    os.dup2(devnull, sys.stdout.fileno())

def write(s, out=sys.stdout):

    "Write 's' safely to 'out', by default standard output."

    try:
        print(s, file=out)
    except BrokenPipeError:
        handle_broken_pipe()

# vim: tabstop=4 expandtab shiftwidth=4
