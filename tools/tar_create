#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
PROGNAME=`basename "$0"`

ARCHIVE=$1
DATADIR=$2

if [ ! "$ARCHIVE" ] || [ ! -e "$DATADIR" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <archive> <data directory> [ --append ] [ <item> ... ]

Make an archive from files in the indicated data directory using the archive
filename to determine the appropriate compression mode.

If --append is specified, items will be appended to any existing archive
instead of overwriting such an archive.

If items are explicitly specified, only these files will be added to the
archive.
EOF
    exit 1
fi

ARCHIVE=`realpath "$ARCHIVE"`

shift 2

# Interpret options overriding the default behaviour.

if [ "$1" = '--append' ] ; then
    APPEND=$1
    shift 1
else
    APPEND=
fi

# Determine the archiving mode.

if [ "$APPEND" ] ; then
    MODE="r"
else
    MODE="c"
fi

# Determine the location and name of the data directory.

DATAPARENT=`realpath "$DATADIR/.."`

# Determine compression usage.

COMPRESSION=`"$THISDIR/tar_flag" "$ARCHIVE"`

if [ ! "$APPEND" ] ; then
    FLAG=$COMPRESSION
else
    FLAG=
fi

# Archive specified members within the indicated directory.

if [ "$1" ] ; then

    for MEMBER in $* ; do
        MEMBER=`realpath "$MEMBER"`

        if [ -e "$MEMBER" ] ; then
            DATANAME=${MEMBER#$DATAPARENT/}
            tar "$MODE""$FLAG"f "$ARCHIVE" -C "$DATAPARENT" "$DATANAME"
        fi
        MODE="r"
    done

# Archive the indicated directory.

else
    DATANAME=`basename "$DATADIR"`
    tar "$MODE""$FLAG"f "$ARCHIVE" -C "$DATAPARENT" "$DATANAME"
fi

# vim: tabstop=4 expandtab shiftwidth=4
