#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
TAB=`printf '\t'`



# Filename processing functions.

basename_and_name()
{
    echo "`basename \"$1\" $2`${TAB}$1"
}



# Main program.

ARCHIVED_PFM_DATA=$1
UPDATED_PFM_DATA=$2
OUTPUT_DIR=$3
PROCESSES=${4:-1}

if [ ! -e "$ARCHIVED_PFM_DATA" ] || [ ! -e "$UPDATED_PFM_DATA" ] ; then
    echo "No valid input directories containing PFM data provided for logo generation." 1>&2
    exit 1
fi

if [ ! "$OUTPUT_DIR" ] ; then
    echo "An output directory is required." 1>&2
    exit 1
fi

if [ ! -e "$OUTPUT_DIR" ] ; then
    mkdir -p "$OUTPUT_DIR"
fi

THISDIR=`realpath "$THISDIR"`
ARCHIVED_PFM_DATA=`realpath "$ARCHIVED_PFM_DATA"`
UPDATED_PFM_DATA=`realpath "$UPDATED_PFM_DATA"`
OUTPUT_DIR=`realpath "$OUTPUT_DIR"`

# Use temporary files.

LOGOS=`mktemp`
PFMS=`mktemp`

trap "rm \"$LOGOS\" \"$PFMS\"" EXIT

# Obtain already-generated logo details.

for FN in "$OUTPUT_DIR/"*".rc.png" ; do
    if [ ! -e "$FN" ] ; then
        break
    fi
    basename_and_name "$FN" .rc.png
done \
| LC_ALL=C sort \
> "$LOGOS"

for FN in "$ARCHIVED_PFM_DATA/"*".pfm" "$UPDATED_PFM_DATA/"*".pfm" ; do
    if [ ! -e "$FN" ] ; then
        break
    fi
    basename_and_name "$FN" .pfm
done \
| LC_ALL=C sort \
> "$PFMS"

# Find PFMs without logos.
# Distribute the processing of PFM files to a number of processes.

export R_LIBS="$BASEDIR/lib/usr/local/lib/R/library"

  LC_ALL=C join -t "`printf '\t'`" -1 1 -2 1 -v 1 "$PFMS" "$LOGOS" \
| cut -f 2 \
| xargs -I{} -P "$PROCESSES" sh -c "Rscript \"$THISDIR/create_JASPAR2020_logos.R\" {} \"$OUTPUT_DIR\" > /dev/null 2>&1"

# vim: tabstop=4 expandtab shiftwidth=4
