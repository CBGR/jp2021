Updating for a New Release
==========================

When a new release of JASPAR is to be prepared, a number of updates will need
to be performed on resources found in this distribution so that the processing
activity will produce appropriate products for deployment.


Data/Metadata Resources
-----------------------

In the ``data`` directory, a number of files will need updating.

The ``portal_release`` file will need modifying, with a new line (or record)
for the details of the release.

Any adjustments to taxonomy group labelling will need to be incorporated into
the ``tax_groups`` file.

Any inconsistencies between curation data annotations and the actual
annotations employed in the database will need to be resolved by adding
appropriate records to the ``tag_synonyms`` file.

Curation Spreadsheet
--------------------

For JASPAR 2022, a curation spreadsheet - ``JASPAR2022_curation.ods`` - was
provided as an Open Document Format file, having been exported from a Google
online document.

Such a file will need to be provided for a new release, featuring
a table (or tab) containing curation details and a table containing deleted
profiles. The precise naming of this file and the tables is defined in the
``Makefile`` which will need adjusting to recognise the form of the new data.

Changelog Document
------------------

For JASPAR 2022, a changelog document - ``Changelog_JASPAR_2022.odt`` - was
provided as an Open Document Format file, having been exported from a Google
online document.

A similar file, incorporating change details for a new release, will need to
be provided so that the Web application can report these details, with its
details being defined in the ``Makefile`` as appropriate.

TFFM Data
---------

In the 2022 release, TFFM data was prepared after the initial update process
involving the new curation data (or metadata) and new profile data. The
identifiers associated with newly incorporated profiles were then used in the
TFFM data preparation activity, and a separate processing step brought this
TFFM data back into the more general activity documented here.

Since the ``Makefile`` references the external TFFM data, the location of such
data needs to be adjusted appropriately in that file.
