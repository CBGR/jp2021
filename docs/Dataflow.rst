Dataflow
========

This software is intended to process curation data provided in a spreadsheet,
producing an updated form of the JASPAR database. The curation spreadsheet,
provided as an Open Document Format file, will have a filename such as
``JASPAR2022_curation.ods`` and will contain various tabs including one
providing curation records with a name such as ``JASPAR_2022_curation_table``.
These records will be extracted from the spreadsheet file and processed
further.

For earlier JASPAR releases, a different data preparation workflow was used.
`Some notes`_ about this workflow were written to inform the construction of the
software in this distribution.

.. _`Some notes`: Legacy.rst

.. contents::

Overview
--------

The following diagram summarises the different activities involved in
transforming existing and new curation data into the output data required by
JASPAR.

.. image:: Dataflow.svg

Logos and PFM Data
''''''''''''''''''

Logo and PFM data preparation occurs as a consequence of metadata preparation
and can be considered separately.

.. image:: Dataflow-data.svg

Synonyms
''''''''

The synonym preparation occurs as a consequence of metadata preparation and
can be considered separately.

.. image:: Dataflow-synonyms.svg

TFFMs
'''''

TFFM preparation occurs as a consequence of metadata preparation, with the
existing TFFM file details being used as an input to new metadata preparation.

.. image:: Dataflow-tffm.svg

Other Resources
'''''''''''''''

Also part of the data preparation workflow are a number of simple processes
involving the preparation of changelogs, clustering data and wordclouds.

.. image:: Dataflow-simple.svg

The details of the different activities are described below.

Input and Output Data
---------------------

In general, data provided as input to this process is cached and then
processed. Upon completing the processing activity, the installation of the
results will involve these results plus the input data being copied to an
output location.

Input data typically resides in a referenced data directory defined by the
``JASPAR_DATA_DIR`` variable. The ``JASPAR_ALL_DATA`` variable references other
variables that provide the input filename details, with these other variables
including the following:

- ``CHANGELOG_DATA``
- ``CURATION_DATA``
- ``CHANGELOG_REVISION_DATA``
- ``CURATION_REVISION_DATA``
- ``JASPAR_ARCHETYPES_DATA``
- ``JASPAR_ARCHIVED_DATA``
- ``JASPAR_CLUSTERING_DATA``
- ``JASPAR_DOWNLOAD_DATA``
- ``JASPAR_EXISTING_DATA``
- ``JASPAR_MATRIX_HISTORY``
- ``JASPAR_SITE_DATA``
- ``JASPAR_EXISTING_TFFM_DATA``
- ``JASPAR_INCOMING_TFFM_DATA``
- ``JASPAR_WORDCLOUDS_DATA``

Additionally, some external data files may be present in the referenced data
directory:

- ``TAX_DATA``
- ``GENE_INFO``
- ``UNIPROT_SPROT``

The caching of input data involves making symbolic links from within the
working directory to the appropriate input files. Variables employing the
``_CACHED`` suffix are used for this purpose. For example:

- ``UNIPROT_SPROT_CACHED``

Where processing has been completed previously, with installation having been
performed, any "old" input data may be retrieved from the output location. A
number of variables exist to identify such inputs. These will employ the
``_OLD`` suffix. For example:

- ``UNIPROT_SPROT_OLD``

The following priority is used when selecting input data of a particular type:

#. Explicitly indicated input data using ``CURATION_DATA`` (and so on)
#. Cached input data in the working directory
#. Data in the referenced data directory
#. Previously installed "old" input data

External data files may be downloaded and cached if they are otherwise absent.

Generally, processing should rely on the referenced data directory to contain
the necessary files and it should not be necessary to specify input data
explicitly.

Curation Data
-------------

Curation is centred on a spreadsheet containing several tabs or tables. The
pertinent table for this software is the table establishing the curated
records for inclusion in JASPAR and their categorisation. This curation
spreadsheet is obtained as an Open Document Format (ODF) spreadsheet file with
a file suffix or extension of ``.ods``. Note that this is *not* the same as
Office Open XML (OOXML) format typically produced by Microsoft products and
having a ``.docx`` file extension.

Format Conversion
'''''''''''''''''

A tool called ``ods2tsv`` is used to extract data from the spreadsheet and to
make it available as simple, tab-separated data. This tool relies on the
Python ``odfpy`` package for querying and interpretation of the spreadsheet
document archive.

Record Handling
---------------

Curation records have the following categories:

=============== =========================== ==================================
Category        Effect on existing profile  New profile details
=============== =========================== ==================================
Assignment      Replaced                    ``MA`` profile, new PFM data
Non-validated   (No existing profile)       ``UN`` profile, new PFM data
Updated         (Old versions retained)     New version, new PFM data
Validated       Replaced                    ``MA`` profile, existing PFM data
=============== =========================== ==================================

Assignment records add or replace existing profile information. When replacing
an existing profile, such a profile will typically have an identifier with a
prefix other than ``MA``, with the replacement profile employing ``MA``.

Apart from updated records, any new profiles will use newly assigned sequence
numbers for the appropriate prefix. These follow on from the largest sequence
numbers known for each prefix.

.. image:: Curation_records.svg
