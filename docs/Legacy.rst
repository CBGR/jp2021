Dataflow
========

In the previously used curation notebook, ``load_csv`` is used to interpret a
tab-separated file containing curation records. This loaded data is then used
with various functions; these are mentioned below in descriptions of the
different processing activities.

Matrix Data
-----------

The ``load_matrix_data_2020`` function takes the loaded records and uses the
"ID" and "PWM" columns to load PWM files, inserting matrix data into the
database. The ``load_matrix_data`` function does the same for older data
versions.

The "PWM" column contains filenames, each referencing a PWM filename that
typically ends in ``.jaspar``, although there are also ``.jaspar.trimmed``
files, also apparently containing PWM data. In the new curation table,
"NEW_motif_path" appears to be the location of the matrix data.

The ``matrix_data`` table contains records with the following columns:

::

  id, row, col, val

Here, ``row`` is 'A', 'C', 'G' or 'T', col is a number in the range 1..n,
where n is the number of columns in the matrix, and ``val`` is the value in
the matrix cell at (``row``, ``col``). The ``id`` column contains a unique
identifier for the matrix.

Matrix Profiles
---------------

In the previously used curation notebook, the ``add_matrix`` function adds
profile information from the loaded records.

The ``matrix`` table contains records with the following columns:

::

  id, collection, base_id, version, name

Here, the ``id`` column is the unique identifier for the matrix. The
``base_id``, ``version`` and ``name`` columns are supplied by the
"current_BASE_ID", "current_VERSION" and "TF NAME" columns, respectively, in
the input data.

Matrix Proteins (Accessions)
----------------------------

In the previously used curation notebook, the ``add_protein`` function adds
protein accession details from the loaded records, with
``update_matrix_protein`` splitting compound accessions as noted below.

The ``matrix_protein`` table contains records with the following columns:

::

  id, acc

Again, the ``id`` column is the unique identifier for the matrix. The ``acc``
column is supplied by the "Uniprot" column in the input data.

Additional accession information appears to be obtained from any indicated
UniProt accession, where the "::" separator may have been used to concatenate
multiple accessions. These are extracted and recorded separately in the
``matrix_protein`` table. (Currently, concatenation of protein accessions does
not appear to be done.)

Matrix Species
--------------

In the previously used curation notebook, the ``add_species`` function adds
species-related information from the loaded records.

The ``matrix_species`` table contains records with the following columns:

::

  id, tax_id

Again, the ``id`` column is the unique identifier for the matrix. The
``tax_id`` column is supplied by the "TAX_ID" column in the input data.

Matrix Annotations
------------------

In the previously used curation notebook, the ``add_matrix_annotation_tags``
function adds annotations for a given profile from the loaded records, with
``update_annotations`` splitting compound annotations as noted below.

The following tags are defined in the notebook:

::

  ['class', 'family', 'tfbs_shape_id', 'remap_tf_name', 'type', 'tax_group',
   'source', 'medline', 'comment']

The ``matrix_annotation`` table contains records with the following columns:

::

  id, tag, val

Again, the ``id`` column is the unique identifier for the matrix. The ``tag``
column is supplied by a column with one of the names given above. The ``val``
column is supplied with the value from the column selected for the tag.

In the current data, the following tag columns appear to be provided, presented
below with the apparently equivalent tag in the existing data:

=================== =================
Column              Apparent Tag
=================== =================
``class``           ``class``
``family``          ``family``
``TFBSshape ID``    ``tfbs_shape_id``
``TF NAME``         ``remap_tf_name``
``Data``            ``type``
``Source``          ``source``
``Validation``      ``medline``
``Comment``         ``comment``
=================== =================

Compound annotation information appears to be encoded using the "::" separator
to concatenate multiple annotation values. These are extracted and recorded
separately for the tag in the ``matrix_annotation`` table.

The ``map_jaspar_remap`` function in the curation notebook appears to add
annotations for ``remap_tf_name``. The effect is to add the TF name as a value
for each matrix profile.

The ``update_jaspar_class_family`` function appears to propagate ``class`` and
``family`` annotations from the latest version of a matrix profile to all
earlier ones. However, it seems to rely on their being only one ``class`` and
one ``family`` per profile version.

The assertion of any ``class`` or ``family`` consistency between versions as
some kind of policy can be tested with the existing database as follows:

.. code:: sql

  select A.* from (
    select base_id, max(version) as versions
    from matrix
    group by base_id
    having max(version) > 1
    ) as V
  inner join (
    select base_id, version, val
    from matrix
    inner join matrix_annotation as MA
      on matrix.id = MA.id
    where tag = 'class'
    ) as A
    on V.base_id = A.base_id
  left outer join (
    select base_id, version, val
    from matrix
    inner join matrix_annotation as MB
      on matrix.id = MB.id
    where tag = 'class'
    ) as B
    on A.base_id = B.base_id
    and A.val = B.val
    and A.version <> B.version
  where B.val is null;

This query handles multiple values for ``class``, producing results that
indicate inconsistencies such as multiple values being employed for one
version but concatenated values being used for another, missing values for
particular versions, and different values between versions.

Files and Resources
'''''''''''''''''''

The ``move_files_j2020`` function copies and converts files referenced by a
number of columns:

=================== ===============================================
Column              Destination within downloads
=================== ===============================================
``BED``             ``bed_files/<BASE_ID>.<VERSION>.bed``
``FASTA``           ``sites/<BASE_ID>.<VERSION>.sites``
``Centrimo_plots``  ``centrality_files/<BASE_ID>.<VERSION>.png``
=================== ===============================================

The ``BASE_ID`` and ``VERSION`` columns provide matrix profile details. To
produce the output for ``Centrimo_plots``, the indicated files are converted
to PNG format. In the function, the Mac OS-specific ``sips`` tool is used:

.. code:: shell

   sips -s format png <input filename> --out <output filename>

The equivalent ImageMagick command would be this:

.. code:: shell

   convert <input filename> <output filename>

In both cases, the output filename would employ the ``.png`` suffix, this
being particularly important for the ``convert`` invocation unless explicit
format prefixing is used.

The ``Centrimo_pvalue`` column provides the name of a file whose contents
provide a value for a matrix annotation ``centrality_logp``.

Ultimately, the following directories within the JASPAR application's
``download`` directory need to be populated:

===================== =======================================================
Directory             Contents
===================== =======================================================
``all_files``         Position Frequency Matrix files
``bed_files``         Binding site details in BED format
``centrality_files``  PNG files showing ChIP-seq centrality plots
``collections``       Matrix profile details for non-CORE collections
``CORE``              Matrix profile details for the CORE collection
``database``          Database dump files containing the JASPAR metadata
``sites``             Binding site details in FASTA format
``TFFM``              Transcription Factor Flexible Model files
===================== =======================================================
